//! `iqiper-oreille` is a the API Server of `iqiper`
//!
//! ## Crud operation
//!
//! It'll perform any CRUD operation the user is authorized to.
//!
//! ## Authentification
//!
//! `oreille` will forward valid requests for token to `Keycloak` to allow users
//! to get new tokens / refresh old tokens.
//!
//! ## Authorization
//!
//! Every request will be validated against the scopes defined for that route in
//! the `OpenApi` specification

use peau::actix_rt;
use peau::actix_web;
// For linker issue, see : https://github.com/emk/rust-musl-builder#making-diesel-work
#[allow(unused_imports)]
extern crate openssl;
#[macro_use]
#[allow(unused_imports)]
extern crate diesel;
#[macro_use]
extern crate log;
#[macro_use]
extern crate serde_json;

mod activity;
mod activity_template;
mod common;
mod communication;
mod contact;
mod data_state;
mod error;
mod notification;
mod openapi;
mod users;
mod users_utils;
mod validation;

#[cfg(test)]
mod tests;
use crate::data_state::configuration::{self};
use crate::validation::Validator;
use iqiper_mains::jwt_extractor::CoreJWTConfig;
const CONF_FILE: &str = "IQIPER_OREILLE_CONFIG";

#[macro_export]
macro_rules! iqiper_oreille_app {
    ($server_state: expr, $conf: expr, $conf_mains: expr, $openapi_specs: expr, $metrics: expr) => {
        peau::actix_web::App::new()
            .wrap(peau::actix_web::middleware::Logger::default())
            .wrap($metrics)
            .app_data(
                peau::actix_web::web::PathConfig::default().error_handler(|err, _| {
                    crate::error::ResponseError::IllFormedRequestCustom(format!("{}", err)).into()
                }),
            )
            .app_data(
                peau::actix_web::web::QueryConfig::default().error_handler(|err, _| {
                    crate::error::ResponseError::IllFormedRequestCustom(format!("{}", err)).into()
                }),
            )
            .app_data($conf_mains.jwt_config().clone() as CoreJWTConfig)
            .app_data($openapi_specs.clone())
            .app_data($conf_mains.jwt_config().clone() as CoreJWTConfig)
            .data($conf_mains)
            .data($server_state.clone())
            .data(
                crate::contact::ValidatorContact::new()
                    .init()
                    .expect("Failed to create validator"),
            )
            .data(
                crate::activity::ValidatorActivity::new()
                    .init()
                    .expect("Failed to create validator"),
            )
            .data(
                crate::activity_template::ValidatorActivityTemplate::new()
                    .init()
                    .expect("Failed to create validator"),
            )
            .data(
                crate::notification::ValidatorNotification::new()
                    .init()
                    .expect("Failed to create validator"),
            )
            .service(
                peau::actix_web::web::resource(
                    $conf_mains.settings().local_server.local_code_route.clone(),
                )
                .route(peau::actix_web::web::get().to(iqiper_mains::forward_authn_token_query))
                .route(peau::actix_web::web::post().to(iqiper_mains::forward_authn_token_form)),
            )
            .configure(crate::contact::set_routes)
            .configure(crate::activity::set_routes)
            .configure(crate::activity_template::set_routes)
            .configure(crate::notification::set_routes)
            .configure(crate::openapi::set_routes)
            .configure(crate::communication::set_routes)
            .configure(crate::users::set_routes)
            .configure(peau::web::health::alive)
            .configure(peau::web::health::ready)
            .default_service(peau::actix_web::web::route().to(crate::error::default_not_found))
    };
}

fn setup_metrics() -> (
    peau::prometheus::Registry,
    peau::actix_web_prom::PrometheusMetrics,
    peau::actix_web_prom::PrometheusMetrics,
) {
    let shared_registry = peau::prometheus::Registry::new();

    let metrics_private = peau::actix_web_prom::PrometheusMetrics::new_with_registry(
        shared_registry.clone(),
        "oreille_private",
        Some("/-/metrics"),
        None,
    )
    .unwrap();
    let metrics_public = peau::actix_web_prom::PrometheusMetrics::new_with_registry(
        shared_registry.clone(),
        "oreille_public",
        None,
        None,
    )
    .unwrap();
    (shared_registry, metrics_public, metrics_private)
}

#[actix_rt::main]
async fn main() -> Result<(), error::InternalError> {
    env_logger::Builder::from_env(env_logger::Env::default().default_filter_or("debug")).init();
    let config = configuration::get(std::path::Path::new(
        std::env::var(CONF_FILE)
            .unwrap_or("./config.yml".to_string())
            .as_str(),
    ))?;
    let (_metrics_reg, metrics_actix_public, metrics_actix_private) = setup_metrics();
    let server_state = data_state::server_state::ServerState::new(config.clone()).await?;
    let settings_mains = configuration::get_mains(&config, server_state.kcc())?;
    let jwks = iqiper_mains::request_jwks(
        server_state.keycloak_client(),
        &settings_mains.idp.jwks_route,
    )
    .await;
    let config_mains = iqiper_mains::AuthConfig::new(
        settings_mains.clone(),
        jwks,
        server_state.keycloak_client().clone(),
    );
    let openapi_specs =
        iqiper_mains::api::read_api_file(config.openapi_specs.as_str()).map_err(|err| {
            error::InternalError::from_config_error(config::ConfigError::Message(format!(
                "Error while parsing OpenApi specs ({}): {:#?}",
                config.openapi_specs.as_str(),
                err
            )))
        })?;
    peau::web::health::set_ready();
    peau::web::health::set_alive(true);
    let server_state_public = server_state.clone();
    let server_state_private = server_state;
    let openapi_specs_public = openapi_specs.clone();
    let openapi_specs_private = openapi_specs;
    let config_mains_public = config_mains.clone();
    let config_mains_private = config_mains;
    let server_public = actix_web::HttpServer::new(move || {
        iqiper_oreille_app!(
            &server_state_public,
            &conf_pub,
            config_mains_public.clone(),
            openapi_specs_public,
            metrics_actix_public.clone()
        )
    });
    let server_private = actix_web::HttpServer::new(move || {
        iqiper_oreille_app!(
            &server_state_private,
            &conf_priv,
            config_mains_private.clone(),
            openapi_specs_private,
            metrics_actix_private.clone()
        )
    });
    let server_public =
        peau::web::web::bind_web_server(&config.server_public, server_public)?.run();
    let server_private =
        peau::web::web::bind_web_server(&config.server_private, server_private)?.run();
    Ok(futures::try_join!(server_public, server_private)
        .map(|_| ())
        .map_err(|err| error::InternalError::from_io(err, "await"))?)
}
