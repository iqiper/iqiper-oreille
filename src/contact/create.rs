use super::*;

/// POST /contact
/// Create a new contact.
#[iqiper_mains_route(method = "post", route = "/contact")]
async fn route_create_contact(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    data_valid: web::Data<ValidatorContact>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindContact::Create, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let form = contact::NewContact::deserialize(uid, body.0)?;
    if let Some(target_uid) = form.target_uid {
        if target_uid == uid {
            return Err(ResponseError::IllFormedRequestCustom(String::from(
                "The target_uid can't be oneself",
            )));
        }
        let kcc = data_server.get_ref().kcc();
        crate::users_utils::check_exists_in_keycloak(&connection, &kcc, &target_uid).await?;
    }
    let res = contact::create(&connection, form)?;
    Ok(HttpResponse::Created().json(res))
}
