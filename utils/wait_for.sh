echo "Waiting for $1...."
COUNTER=240
until $(curl --output /dev/null --silent --head --fail $1); do
    printf '.'
    sleep 1
	COUNTER=$(($COUNTER-1))
	if [ $COUNTER -eq 0 ];
	then
		echo "$1 TIMEOUT"
		exit 1
	fi
done
echo "$1 is ready"
