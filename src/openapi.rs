use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use peau::actix_web::http::StatusCode;
use peau::actix_web::{web, HttpResponse};

/// Get the OpenApi definition of `Oreille` cached in the server state
///
/// # Return value
/// An HTTP Response containing the OpenApi definition in JSON
async fn get_local_openapi(
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    Ok(HttpResponse::build(StatusCode::OK).json(data_server.into_inner().openapi_specs()))
}

/// Set the route for the openapi feature
///
/// # Arguments
/// - `cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(web::resource("/swagger.json").route(web::get().to(get_local_openapi)))
        .service(web::resource("/openapi.json").route(web::get().to(get_local_openapi)))
        .service(web::resource("/openapi.json").route(web::get().to(get_local_openapi)))
        .service(
            web::resource("/.well-known/openapi.json").route(web::get().to(get_local_openapi)),
        );
}
