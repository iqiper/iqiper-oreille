use super::*;

/// DELETE /model/activity/{id}/constraint
/// Delete an activity contact
#[iqiper_mains_route(method = "delete", route = "/model/activity/{id}/contact/{cid}")]
async fn route_delete_activity_contact_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let nb = db::activity_contact_template::delete(&connection, &uid, &path.id, &path.cid)?;
    match nb {
        1 => Ok(HttpResponse::Ok().json(
            db::activity_contact_template::ActivityContactTemplateId {
                activity_template_id: path.id,
                contact_id: path.cid,
            },
        )),
        0 => Err(ResponseError::NotFound(format!(
            "activity_contact with id '{}'",
            path.id.to_string()
        ))),
        _ => Err(ResponseError::InternalServerError),
    }
}

/// DELETE /model/activity/{id}/constraint
/// Delete an activity constraint
#[iqiper_mains_route(method = "delete", route = "/model/activity/{id}/constraint/{cid}")]
async fn route_delete_activity_constraint_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    db::activity_constraint_template::delete(&connection, &uid, &path.id, &path.cid)?;
    Ok(HttpResponse::Ok().json(Id { id: path.cid }))
}

/// DELETE /model/activity/{id}
/// Delete an activity
#[iqiper_mains_route(method = "delete", route = "/model/activity/{id}")]
async fn route_delete_activity_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let nb = db::activity_template::delete(&connection, &uid, &path.id)?;
    match nb {
        1 => Ok(HttpResponse::Ok().json(path.into_inner())),
        0 => Err(ResponseError::NotFound(format!(
            "activity with id '{}'",
            path.id.to_string()
        ))),
        _ => Err(ResponseError::InternalServerError),
    }
}
