/// Contains critical errors that will stop the service.
mod internal_error;
/// Contains error that would be returned to the user of the service.
mod response_error;

pub use internal_error::InternalError;
pub use response_error::default_not_found;
pub use response_error::ResponseError;
