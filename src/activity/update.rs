use super::*;

/// Update an activity constraint
///
/// # Arguments
/// - `conn` - The connection to the database
/// - `uid` - The user id
/// - `activity_id` - The activity id
/// - `constraint_id` - The activity constraint id
/// - `constraint` - The constraint object to create
///
/// # Return Value
/// A result to either the UpdateResponse or an database error
pub fn update_activity_constraint(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    constraint_id: &Uuid,
    constraint: ConstraintCreator,
) -> Result<UpdateResponse, db::error::IqiperDatabaseError> {
    match constraint {
        ConstraintCreator::Timer(x) => activity_constraint::modify(
            conn,
            uid,
            activity_id,
            constraint_id,
            activity_constraint::NewActivityConstraint {
                type_: ActivityConstraintType::Timer,
                activity_id: *activity_id,
                alert_message: x.alert_message,
                start_date: x.start_date,
                end_date: x.end_date,
                notify_date: x.notify_date,
            },
        ),
    }
}

/// PUT /activity/{id}/name
/// Change an activity name
#[iqiper_mains_route(method = "put", route = "/activity/{id}/name")]
async fn route_update_activity_name(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivity>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::UpdateName, &body)?;
    let body = UpdateName::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let tmp = db::activity::update_name(&connection, &uid, &path.into_inner().id, body.name)?;
    Ok(HttpResponse::Ok().json(tmp))
}

/// PUT /activity/{id}/message
/// Change an activity message
#[iqiper_mains_route(method = "put", route = "/activity/{id}/alert_message")]
async fn route_update_activity_message(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivity>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::UpdateAlertMessage, &body)?;
    let body = UpdateAlertMessage::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let tmp = db::activity::update_alert_message(
        &connection,
        &uid,
        &path.into_inner().id,
        body.alert_message,
    )?;
    Ok(HttpResponse::Ok().json(tmp))
}

/// PUT /activity/{id}/icon
/// Change an activity icon
#[iqiper_mains_route(method = "put", route = "/activity/{id}/icon")]
async fn route_update_activity_icon(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivity>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::UpdateIcon, &body)?;
    let body = UpdateIcon::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(db::activity::update_icon(
        &connection,
        &uid,
        &path.into_inner().id,
        body.icon,
    )?))
}

/// PUT /activity/{id}/constraint/{cid}
/// Modify an activity constraint
#[iqiper_mains_route(method = "put", route = "/activity/{id}/constraint/{cid}")]
async fn route_update_activity_constraint(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivity>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::CreateConstraint, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(update_activity_constraint(
        &connection,
        &uid,
        &path.id,
        &path.cid,
        ConstraintCreator::deserialize(body.into_inner())?,
    )?))
}
