use crate::common::*;
use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::users_utils::extract_uid;
use db::communication::SelectCommunication;
use db::communication_log::SelectCommunicationLog;
use iqiper_mains;
use iqiper_mains::iqiper_mains_route;
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};
use uuid::Uuid;
mod read;

/// Set the route for the communication feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(read::read_all_communication_for_constraint)
        .service(read::read_all_communication_for_activity)
        .service(read::read_by_id)
        .service(read::read_recipient_log)
        .service(read::read_recipient);
}
