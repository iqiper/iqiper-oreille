use crate::data_state::configuration::{Configuration, SSLModeType};
use crate::error::InternalError;
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use getset::Getters;
use iqiper_tealer::oidc::{ClientId, ClientSecret, IssuerUrl};
use iqiper_tealer::{CoreIqiperTealerActor, IqiperTealer};
use kcc::KeycloakClient;
use log::info;
use std::sync::{Arc, RwLock, RwLockReadGuard};

#[derive(Clone, Getters)]
#[getset(get = "pub")]
pub struct ServerState {
    conf: Arc<RwLock<Configuration>>,
    db_pool: Arc<RwLock<Pool<ConnectionManager<PgConnection>>>>,
    kcc: Arc<KeycloakClient>,
    openapi_specs: Arc<iqiper_mains::api::OpenAPIDocument>,
    keycloak_client: peau::reqwest::Client,
    redis: Arc<visage::r2d2::Pool<visage::redis::Client>>,
}

/// Initialize the database pool using the configuration
///
/// # Arguments
/// - `conf` - The configuration
///
/// # Return value
/// The database connection pool
fn init_db_pool(
    conf: &Configuration,
) -> Result<r2d2::Pool<diesel::r2d2::ConnectionManager<diesel::PgConnection>>, InternalError> {
    info!("Initializing database connection pool...");
    let conf_db = &conf.database;
    let mut connection_string = format!(
        "postgresql://{}:{}@{}:{}/{}?sslmode={}",
        conf_db.user,
        conf_db.password,
        conf_db.host,
        conf_db.port,
        conf_db.database,
        conf_db
            .sslmode
            .as_ref()
            .unwrap_or(&SSLModeType::Prefer)
            .to_string()
    );
    if let Some(sslcert) = &conf_db.sslcert {
        connection_string.push_str(format!("&sslcert={}", sslcert).as_str());
    }
    if let Some(sslkey) = &conf_db.sslkey {
        connection_string.push_str(format!("&sslkey={}", sslkey).as_str());
    }
    if let Some(sslrootcert) = &conf_db.sslrootcert {
        connection_string.push_str(format!("&sslrootcert={}", sslrootcert).as_str());
    }
    let manager = ConnectionManager::new(connection_string);
    let db_pool = Pool::builder()
        .error_handler(Box::new(diesel::r2d2::LoggingErrorHandler {}))
        .event_handler(Box::new(db::events_logger::IqiperDatabaseEventHandler {}))
        .max_size(conf_db.connection_number.parse::<u32>().unwrap_or(16))
        .build(manager)?;
    if conf.database.run_migrations {
        info!("Applying database migrations...");
        db::diesel_migrations::run_migrations(
            &db_pool
                .get()
                .expect("Can't get a connection to apply migrations"),
            db::embedded_migrations::get_migrations().iter().map(|v| *v),
            &mut std::io::stdout(),
        )
        .expect("Can't apply migrations");
        if conf.database.quit_after_migrations.is_some()
            && conf.database.quit_after_migrations.unwrap()
        {
            info!("Quitting after migrations");
            std::process::exit(0);
        }
    }
    Ok(db_pool)
}

/// Initialize the Keycloak Client class
///
/// # Arguments
/// - `config` - The configuration
/// - `reqwest_client` - The reqwest client used to build the Keycloak Client
///
/// # Return value
/// The Keycloak Client
async fn init_kcc(config: &Configuration, reqwest_client: peau::reqwest::Client) -> KeycloakClient {
    info!("Initializing the tealer...");
    let tealer = IqiperTealer::new_client_grant(
        reqwest_client.clone(),
        IssuerUrl::new(format!(
            "{}://{}:{}/auth/realms/{}",
            config.keycloak.http.scheme,
            config.keycloak.http.host,
            config.keycloak.http.port,
            config.keycloak.realm
        ))
        .expect("Should've correctly built the IdP URL"),
        ClientId::new(config.keycloak.client_id.clone()),
        Some(ClientSecret::new(config.keycloak.client_secret.clone())),
    )
    .await
    .expect("Should've discovered the IdP correctly");
    info!("Initializing KCC...");
    let tealer_actor = Arc::new(RwLock::new(CoreIqiperTealerActor::new(tealer)));
    let kcc = KeycloakClient::new_shared(
        tealer_actor.clone(),
        reqwest_client.clone(),
        format!(
            "{}://{}:{}",
            config.keycloak.http.scheme, config.keycloak.http.host, config.keycloak.http.port
        ),
        config.keycloak.realm.clone(),
    );
    kcc
}

impl ServerState {
    /// Create a new server state object
    ///
    /// # Arguments
    /// - `conf` - The configuration
    ///
    /// # Return value
    /// The server state
    pub async fn new(conf: Configuration) -> Result<Self, InternalError> {
        let db_pool = init_db_pool(&conf)?;
        let openapi_specs =
            iqiper_mains::api::read_api_file(conf.openapi_specs.as_str()).map_err(|err| {
                InternalError::from_config_error(config::ConfigError::Message(format!(
                    "Error while parsing OpenApi specs ({}) : {:#?}",
                    conf.openapi_specs.as_str(),
                    err
                )))
            })?;
        let keycloak_client = peau::web::client::init_reqwest_client(&conf.keycloak.http)?;
        let kcc = init_kcc(&conf, keycloak_client.clone()).await;
        Ok(ServerState {
            conf: Arc::new(RwLock::new(conf.clone())),
            openapi_specs: Arc::new(openapi_specs),
            db_pool: Arc::new(RwLock::new(db_pool)),
            kcc: Arc::new(kcc),
            keycloak_client,
            redis: Arc::new(visage::connections::redis::get_redis_connection_pool(
                &conf.redis,
            )?),
        })
    }

    /// Get a new connection from the database pool
    ///
    /// # Return value
    /// A read lock guard wrapped connection
    pub fn db_pool_read(&self) -> RwLockReadGuard<'_, Pool<ConnectionManager<PgConnection>>> {
        self.db_pool.read().expect("Database pool handle poisoned")
    }
}
