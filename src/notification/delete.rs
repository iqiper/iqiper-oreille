use super::*;

/// DELETE /notification
/// Delete a notification
#[iqiper_mains_route(method = "delete", route = "/notification")]
async fn route_delete_notif(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    data_valid: web::Data<ValidatorNotification>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindNotification::Delete, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let form: TokenDevice = serde_json::from_value(body.0)?;
    let res = fcm_token::delete_fcm_token(&connection, &uid, &form.device_id)?;
    Ok(HttpResponse::Ok().json(res))
}
