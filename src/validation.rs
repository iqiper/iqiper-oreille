use crate::error::{InternalError, ResponseError};
use jsonschema::{Draft, JSONSchema};
use serde_json::Value;
use std::collections::HashMap;
use std::hash::Hash;

pub trait SchemaKind: Eq + Hash + Sized {
    fn lst_entries() -> Vec<(Self, String, String)>;
}

pub trait Validator: Sized {
    type Kind: SchemaKind;

    fn get_map(&self) -> &HashMap<Self::Kind, Value>;
    fn get_map_mut(&mut self) -> &mut HashMap<Self::Kind, Value>;

    /// Initialize the validator
    ///
    /// # Return value
    /// The validator object
    fn init(mut self) -> Result<Self, InternalError> {
        let map = self.get_map_mut();
        for (kind, path, value) in Self::Kind::lst_entries() {
            map.insert(
                kind,
                serde_json::from_str(value.as_str())
                    .map_err(|err| InternalError::from_json(err, path.as_str()))?,
            );
        }
        Ok(self)
    }

    /// Validate an object against the JSON schema
    ///
    /// # Arguments
    /// - `kind` - On what kind of schema to validate against
    /// - `value` - The serde value to validate
    ///
    /// # Return value
    /// Nothing
    fn validate(&self, kind: &Self::Kind, value: &Value) -> Result<(), ResponseError> {
        let schema_value = self
            .get_map()
            .get(kind)
            .ok_or(ResponseError::InternalServerError)?;
        let schema = JSONSchema::compile(schema_value, Some(Draft::Draft7))
            .or(Err(ResponseError::InternalServerError))?;
        schema.validate(value).and(Ok(())).or_else(|errs| {
            let mut err_msg = String::new();
            for (i, err) in errs.enumerate() {
                err_msg.push_str(format!("[{}] {}\n", i, err.to_string()).as_str());
            }
            Err(ResponseError::IllFormedRequestCustom(err_msg))
        })
    }
}
