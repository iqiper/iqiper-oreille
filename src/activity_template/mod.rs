mod create;
mod delete;
mod read;
mod update;

use crate::common::*;
use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::validation::{SchemaKind, Validator};
use iqiper_mains;
use iqiper_mains::iqiper_mains_route;
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};

use db::activity_constraint_template::{self, SelectActivityConstraintTemplate};
use db::activity_contact_template::{self, ActivityContactTemplate};
use db::activity_template::{self, NewActivityTemplate, NewActivityTemplateWithoutUid};
use db::common::{IdTsResponse, UpdateResponse};

use crate::users_utils::extract_uid;
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;

use db::enums::ActivityConstraintTemplateType;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

use uuid::Uuid;

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum ConstraintCreator {
    Timer(CreateConstraintTimer),
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct CreateConstraintTimer {
    pub alert_message: Option<String>,
    pub start_offset: i32,
    pub end_offset: i32,
    pub notify_offset: Option<i32>,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateName {
    pub name: String,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateMessage {
    pub alert_message: Option<String>,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateIcon {
    pub icon: Option<i32>,
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum KindActivityTemplate {
    Create,
    UpdateName,
    UpdateMessage,
    UpdateIcon,
    CreateConstraint,
}

impl SchemaKind for KindActivityTemplate {
    fn lst_entries() -> Vec<(Self, String, String)> {
        vec![
            (
                KindActivityTemplate::Create,
                format!("{}", "activity_template/create.json"),
                include_str!("../../schemas/activity_template/create.json").to_string(),
            ),
            (
                KindActivityTemplate::UpdateName,
                format!("{}", "activity_template/update_name.json"),
                include_str!("../../schemas/activity_template/update_name.json").to_string(),
            ),
            (
                KindActivityTemplate::UpdateMessage,
                format!("{}", "activity_template/update_alert_message.json"),
                include_str!("../../schemas/activity_template/update_alert_message.json")
                    .to_string(),
            ),
            (
                KindActivityTemplate::UpdateIcon,
                format!("{}", "activity_template/update_icon.json"),
                include_str!("../../schemas/activity_template/update_icon.json").to_string(),
            ),
            (
                KindActivityTemplate::CreateConstraint,
                format!("{}", "activity_template/create_constraint.json"),
                include_str!("../../schemas/activity_template/create_constraint.json").to_string(),
            ),
        ]
    }
}

#[derive(Clone)]
pub struct ValidatorActivityTemplate {
    schemas_values: HashMap<KindActivityTemplate, Value>,
}

impl Validator for ValidatorActivityTemplate {
    type Kind = KindActivityTemplate;

    fn get_map(&self) -> &HashMap<Self::Kind, Value> {
        &self.schemas_values
    }
    fn get_map_mut(&mut self) -> &mut HashMap<Self::Kind, Value> {
        &mut self.schemas_values
    }
}

impl ValidatorActivityTemplate {
    pub fn new() -> Self {
        ValidatorActivityTemplate {
            schemas_values: HashMap::with_capacity(3),
        }
    }
}

/// Set the route for the activity feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(create::route_create_activity_template)
        .service(create::route_create_activity_contact_template)
        .service(create::route_create_activity_constraint_template)
        .service(read::route_read_activity_template_all)
        .service(read::route_read_activity_template)
        .service(read::route_read_activity_contact_template_all)
        .service(read::route_read_activity_constraint_template_all)
        .service(read::route_read_activity_constraint_template)
        .service(update::route_update_activity_template_message)
        .service(update::route_update_activity_template_icon)
        .service(update::route_update_activity_template_name)
        .service(update::route_update_activity_constraint_template)
        .service(delete::route_delete_activity_template)
        .service(delete::route_delete_activity_contact_template)
        .service(delete::route_delete_activity_constraint_template);
}
