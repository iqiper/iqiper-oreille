use super::*;

/// DELETE /contact/{id}
/// Delete a contact
#[iqiper_mains_route(method = "delete", route = "/contact/{id}")]
async fn route_delete_contact_by_id(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = contact::delete(&connection, &uid, &path.id)?;
    match res {
        0 => Err(ResponseError::NotFound(String::from("id"))),
        1 => Ok(HttpResponse::Ok().json(path.into_inner())),
        _ => Err(ResponseError::InternalServerError),
    }
}
