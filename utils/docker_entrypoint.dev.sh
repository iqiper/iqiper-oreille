#!/bin/bash -x

/usr/src/oreille/target/x86_64-unknown-linux-musl/debug/iqiper-oreille &
if [[ $IQIPER_OREILLE_DATABASE__QUIT_AFTER_MIGRATIONS == "true" ]];
then
	wait %%
	exit 0
fi
while true;
do
	inotifywait /.restart
	kill %%
	wait %%
	/usr/src/oreille/target/x86_64-unknown-linux-musl/debug/iqiper-oreille &

done

