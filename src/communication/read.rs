use super::*;

/// GET /activity/{}/constraint/{}/communication
/// Read all communication for a constraint
#[iqiper_mains_route(
    method = "get",
    route = "/activity/{id}/constraint/{cid}/communication"
)]
async fn read_all_communication_for_constraint(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communications = web::block::<_, Vec<SelectCommunication>, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(db::activity_constraint_communication::select_all(
            &conn, &uid, &path.id, &path.cid,
        )?)
    })
    .await?;
    Ok(HttpResponse::Ok().json(communications))
}

/// GET /activity/{}/communication
/// Read all communication for an activity
#[iqiper_mains_route(method = "get", route = "/activity/{id}/communication")]
async fn read_all_communication_for_activity(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communications = web::block::<_, Vec<SelectCommunication>, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(db::activity_communication::select_all(
            &conn, &uid, &path.id,
        )?)
    })
    .await?;
    Ok(HttpResponse::Ok().json(communications))
}

/// GET /communication/{}
/// Read a communication
#[iqiper_mains_route(method = "get", route = "/communication/{id}")]
async fn read_by_id(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communications = web::block::<_, SelectCommunication, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(db::communication::select_by_id(&conn, &uid, &path.id)?)
    })
    .await?;
    Ok(HttpResponse::Ok().json(communications))
}

/// GET /communication/{}/recipient
/// Read all communication recipients for a communication
#[iqiper_mains_route(method = "get", route = "/communication/{id}/recipient")]
async fn read_recipient(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communications = web::block::<_, Vec<Uuid>, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(
            db::communication_recipient::select_by_communication_id(&conn, &uid, &path.id)
                .map(|x| x.into_iter().map(|x| x.contact_id).collect())?,
        )
    })
    .await?;
    Ok(HttpResponse::Ok().json(communications))
}

/// GET /communication/{}/recipient/{}/log
/// Read all communication recipients for a communication
#[iqiper_mains_route(method = "get", route = "/communication/{id}/recipient/{cid}/log")]
async fn read_recipient_log(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communications = web::block::<_, Vec<SelectCommunicationLog>, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(db::communication_log::select_by_recipient(
            &conn, &uid, &path.id, &path.cid,
        )?)
    })
    .await?;
    Ok(HttpResponse::Ok().json(communications))
}
