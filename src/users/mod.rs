mod delete;

use iqiper_mains;
use iqiper_mains::iqiper_mains_route;

use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::users_utils::extract_uid;
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};

/// Set the route for the self feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(delete::route_delete_self);
}
