#!/bin/bash -ex
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
pushd $DIR/../
mkdir -p tmp_ssl
rm -rf tmp_ssl/*
pushd tmp_ssl
# CA

openssl genrsa -out CA.key 4096
openssl req -x509 -new -nodes -key CA.key -subj "/C=CH/ST=VA/O=Iqiper, SaRL./CN=ca.iqiper.io" -sha256 -days 3 -out CA.crt

# Public

openssl genrsa -out iqiper.io.public.key 2048
openssl req -new -sha256 -key iqiper.io.public.key -subj "/C=CH/ST=VA/O=Iqiper, SaRL./CN=localhost" -out iqiper.io.public.csr
openssl x509 -req -in iqiper.io.public.csr -CA CA.crt -CAkey CA.key -CAcreateserial -out iqiper.io.public.crt -days 3 -sha256
openssl x509 -in iqiper.io.public.crt -out iqiper.io.public.crt.pem -outform PEM
openssl rsa -in iqiper.io.public.key -text -outform PEM -out iqiper.io.public.key.pem
openssl x509 -in CA.crt -out ca.crt.pem -outform PEM
openssl rsa -in CA.key -text -outform PEM -out ca.key.pem
openssl verify -verbose -CAfile ca.crt.pem iqiper.io.public.crt.pem

# Private

openssl genrsa -out iqiper.io.private.key 2048
openssl req -new -sha256 -key iqiper.io.private.key -subj "/C=CH/ST=VA/O=Iqiper, SaRL./CN=localhost" -out iqiper.io.private.csr
openssl x509 -req -in iqiper.io.private.csr -CA CA.crt -CAkey CA.key -CAcreateserial -out iqiper.io.private.crt -days 3 -sha256
openssl x509 -in iqiper.io.private.crt -out iqiper.io.private.crt.pem -outform PEM
openssl rsa -in iqiper.io.private.key -text -outform PEM -out iqiper.io.private.key.pem
openssl x509 -in CA.crt -out ca.crt.pem -outform PEM
openssl rsa -in CA.key -text -outform PEM -out ca.key.pem
openssl verify -verbose -CAfile ca.crt.pem iqiper.io.private.crt.pem

popd
popd
