use std::{fmt, io};

use serde_json::error::Error as JSonError;

/// Wrap all kind of error that could be encoutered by the service.
#[derive(Debug)]
pub enum InternalError {
    /// Wrap an IO error with the name of the file or output linked to the error.
    IO(String, io::Error),
    /// Wrap a Serde error from Json with the name of the file.
    JSon(String, JSonError),
    /// Wrap a Request Error to another service url.
    ReqwestError(peau::reqwest::Error),
    /// Wrap a r2d2 Error.
    R2d2Error(r2d2::Error),
    /// Wrap a KCC Error.
    KCCError(kcc::KCCError),
    /// Wrap an URLParseError.
    URLError(url::ParseError),
    /// Wrap peau error
    PeauError(peau::errors::PeauError),
    /// A problem occured while parsing the configuration
    ConfigError(config::ConfigError),
    /// A problem with redis
    RedisError(visage::redis::RedisError),
    /// A problem with visage
    VisageError(visage::errors::VisageError),
}

impl fmt::Display for InternalError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            InternalError::IO(filename, io_err) => write!(f, "{}: {}", filename, io_err),
            InternalError::JSon(filename, serde_err) => write!(f, "{}: {}", filename, serde_err),
            InternalError::ReqwestError(reqwest_err) => write!(f, "{}", reqwest_err),
            InternalError::R2d2Error(err) => write!(f, "r2d2 error: {}", err),
            InternalError::KCCError(err) => write!(f, "KCC error: {}", err),
            InternalError::URLError(err) => write!(f, "URLError : {}", err),
            InternalError::PeauError(err) => write!(f, "PeauError : {}", err),
            InternalError::ConfigError(err) => write!(f, "ConfigError : {}", err),
            InternalError::RedisError(err) => write!(f, "RedisError : {}", err),
            InternalError::VisageError(err) => write!(f, "VisageError : {}", err),
        }
    }
}

impl std::error::Error for InternalError {}

impl InternalError {
    /// Generate an Error from an IO error and a name.
    ///
    /// # Arguments
    /// * `err` - The IO error to wrap.
    /// * `filename` - the name of the file which caused the error.
    ///
    /// # Return Value
    /// The Error wrap.
    pub fn from_io(err: io::Error, filename: &str) -> Self {
        InternalError::IO(filename.to_owned(), err)
    }

    /// Generate an Error from Serde Json error and a name.
    ///
    /// # Arguments
    /// * `err` - The Serde Json error to wrap.
    /// * `filename` - the name of the file which caused the error.
    ///
    /// # Return Value
    /// The Error wrap.
    pub fn from_json(err: JSonError, filename: &str) -> Self {
        InternalError::JSon(filename.to_owned(), err)
    }

    /// Generate an Error from a config error.
    ///
    /// # Arguments
    /// * `err` - The config error to wrap.
    ///
    /// # Return Value
    /// The Error wrap.
    pub fn from_config_error(err: config::ConfigError) -> Self {
        InternalError::ConfigError(err)
    }
}

impl From<r2d2::Error> for InternalError {
    fn from(err: r2d2::Error) -> Self {
        InternalError::R2d2Error(err)
    }
}

impl From<kcc::KCCError> for InternalError {
    fn from(err: kcc::KCCError) -> Self {
        InternalError::KCCError(err)
    }
}

impl From<url::ParseError> for InternalError {
    fn from(err: url::ParseError) -> Self {
        InternalError::URLError(err)
    }
}

impl From<peau::errors::PeauError> for InternalError {
    fn from(err: peau::errors::PeauError) -> Self {
        InternalError::PeauError(err)
    }
}

impl From<config::ConfigError> for InternalError {
    fn from(err: config::ConfigError) -> Self {
        InternalError::ConfigError(err)
    }
}

impl From<peau::reqwest::Error> for InternalError {
    fn from(err: peau::reqwest::Error) -> Self {
        InternalError::ReqwestError(err)
    }
}

impl From<visage::redis::RedisError> for InternalError {
    fn from(err: visage::redis::RedisError) -> Self {
        InternalError::RedisError(err)
    }
}

impl From<visage::errors::VisageError> for InternalError {
    fn from(err: visage::errors::VisageError) -> Self {
        InternalError::VisageError(err)
    }
}
