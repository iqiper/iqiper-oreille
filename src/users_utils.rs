use crate::error::ResponseError;
use db::distant_user;
use log::error;
use uuid::Uuid;

/// Find or create user in the database
///
/// # Arguments
/// - `conn` - A connection to the database
/// - `uid` - The user id in question
///
/// # Return value
/// The uid
pub fn create_if_not_exists<'a>(
    conn: &diesel::PgConnection,
    uid: &Uuid,
) -> Result<Uuid, ResponseError> {
    let res = distant_user::select_by_id(conn, uid);
    res.or_else(|err| match err {
        db::error::IqiperDatabaseError::DatabaseError(err) => match err {
            diesel::result::Error::NotFound => Ok(distant_user::create_user(conn, uid)?),
            _ => {
                error!("Can't create users in the database");
                Err(ResponseError::InternalServerError)
            }
        },
        _ => {
            error!("Can't create users in the database");
            Err(ResponseError::InternalServerError)
        }
    })?;
    Ok(*uid)
}

/// Check that a user exists in Keycloak
///
/// # Arguments
/// - `conn` - A connection to the database
/// - `uid` - The user id in question
///
/// # Return value
/// The uid
pub async fn check_exists_in_keycloak<'a>(
    conn: &diesel::PgConnection,
    kcc: &kcc::KeycloakClient,
    uid: &Uuid,
) -> Result<Uuid, ResponseError> {
    create_if_not_exists(
        conn,
        kcc.search_user_by_id(*uid)
            .await
            .map(|_x| uid)
            .map_err(|err| match err {
                kcc::KCCError::NotFound(_e) => ResponseError::NotFound(String::from("user")),
                _ => ResponseError::InternalServerError,
            })?,
    )
}

/// Extract the uid from a JWT
///
/// # Arguments
/// - `conn` - A connection to the database
/// - `token` - The valid token parsed by `iqiper-mains`
///
/// # Return value
/// The uid
pub fn extract_uid<'a>(
    conn: &diesel::PgConnection,
    token: &'a iqiper_mains::jwt_extractor::CoreJWTAuth<iqiper_mains::JWTClaims>,
) -> Result<Uuid, ResponseError> {
    create_if_not_exists(conn, &token.jwt().claims.sub)
}
