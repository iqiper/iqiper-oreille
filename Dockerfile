FROM gcr.io/distroless/static:nonroot

COPY --chown=nonroot:nonroot ./target/x86_64-unknown-linux-musl/release/iqiper-oreille /app/iqiper-oreille

WORKDIR /app                                

EXPOSE 8000 8443 9000 9443

ENTRYPOINT ["/app/iqiper-oreille"]
