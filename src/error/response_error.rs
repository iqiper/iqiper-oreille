use db::error::IqiperDatabaseError;
use diesel::result::{DatabaseErrorKind as DieselErrorKind, Error as DieselError};
use peau::actix_web::error::BlockingError;
use peau::actix_web::ResponseError as ResponseErrorTrait;
use peau::actix_web::{
    error,
    http::{header, StatusCode},
    HttpResponse,
};
use std::fmt;

pub async fn default_not_found() -> peau::actix_web::HttpResponse {
    ResponseError::NotFound(String::from("resource")).error_response()
}

/// An error enum outputing a json for the user.
#[derive(Debug)]
pub enum ResponseError {
    ExpiredDocument,
    IllFormedRequestSerde(serde_json::Error),
    IllFormedRequestCustom(String),
    NotFound(String),
    ConflictingResources(String),
    InternalServerError,
    DistantServerError,
    ServiceUnavailable,
}

impl ResponseError {
    fn json_body(&self) -> serde_json::Value {
        match self {
            ResponseError::IllFormedRequestSerde(err) => json!({
                "error": {
                    "code": 400,
                    "message": format!("Unable to deserialize request : {}", err)
                }
            }),
            ResponseError::IllFormedRequestCustom(err) => json!({
                "error": {
                    "code": 400,
                    "message": err
                }
            }),
            ResponseError::ExpiredDocument => json!({
                "error": {
                    "code": 410,
                    "message": "This document has expired"
                }
            }),
            ResponseError::NotFound(data) => json!({
                "error": {
                    "code": 404,
                    "message": format!("{} not found", data)
                }
            }),
            ResponseError::ConflictingResources(resource) => json!({
                "error": {
                    "code": 409,
                    "message": format!("{} is in conflict with the database", resource)
                }
            }),
            ResponseError::InternalServerError => json!({
                "error": {
                    "code": 500,
                    "message": "Something went wrong on our end, please retry later"
                }
            }),
            ResponseError::DistantServerError => json!({
                "error": {
                    "code": 502,
                    "message": "A distant server failed processing this request"
                }
            }),
            ResponseError::ServiceUnavailable => json!({
                "error": {
                    "code": 503,
                    "message": "This request was not processed because a remote service couldn't be contacted"
                }
            }),
        }
    }
}

impl fmt::Display for ResponseError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.json_body())
    }
}

impl error::ResponseError for ResponseError {
    fn error_response(&self) -> HttpResponse {
        HttpResponse::build(self.status_code())
            .set_header(header::CONTENT_TYPE, "application/json")
            .json(self.json_body())
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            ResponseError::IllFormedRequestSerde(_) => StatusCode::BAD_REQUEST,
            ResponseError::IllFormedRequestCustom(_) => StatusCode::BAD_REQUEST,
            ResponseError::NotFound(_) => StatusCode::NOT_FOUND,
            ResponseError::ConflictingResources(_) => StatusCode::CONFLICT,
            ResponseError::InternalServerError => StatusCode::INTERNAL_SERVER_ERROR,
            ResponseError::DistantServerError => StatusCode::BAD_GATEWAY,
            ResponseError::ExpiredDocument => StatusCode::GONE,
            ResponseError::ServiceUnavailable => StatusCode::SERVICE_UNAVAILABLE,
        }
    }
}

impl std::convert::From<DieselError> for ResponseError {
    fn from(err: DieselError) -> Self {
        match err {
            DieselError::DatabaseError(kind, info) => {
                let column = info.column_name().unwrap_or("entry").to_owned();
                match kind {
                    DieselErrorKind::UniqueViolation => ResponseError::ConflictingResources(column),
                    DieselErrorKind::ForeignKeyViolation => ResponseError::NotFound(column),
                    _ => ResponseError::InternalServerError,
                }
            }
            DieselError::InvalidCString(_e) => ResponseError::IllFormedRequestCustom(String::from(
                "The query should not contains nul byte",
            )),
            DieselError::SerializationError(_e) => {
                ResponseError::IllFormedRequestCustom(String::from("Failed to serialize query"))
            }
            DieselError::NotFound => ResponseError::NotFound(String::from("entry")),
            _ => {
                error!("Unknown Diesel error : {}", err);
                ResponseError::InternalServerError
            }
        }
    }
}

impl std::convert::From<IqiperDatabaseError> for ResponseError {
    fn from(err: IqiperDatabaseError) -> Self {
        match err {
            IqiperDatabaseError::DatabaseError(err) => ResponseError::from(err),
            IqiperDatabaseError::LogicError(s) => {
                ResponseError::IllFormedRequestCustom(String::from(s))
            }
            IqiperDatabaseError::TypeChanging => ResponseError::IllFormedRequestCustom(
                String::from("Cannot change type of typed object."),
            ),
            IqiperDatabaseError::ReadOnly => ResponseError::ExpiredDocument,
            IqiperDatabaseError::UnexpectedResult(_, _) => ResponseError::InternalServerError,
        }
    }
}

impl std::convert::From<BlockingError<IqiperDatabaseError>> for ResponseError {
    fn from(err: BlockingError<IqiperDatabaseError>) -> Self {
        match err {
            BlockingError::Canceled => {
                error!("A task has been cancelled {}", err);
                ResponseError::InternalServerError
            }
            BlockingError::Error(x) => ResponseError::from(x),
        }
    }
}

impl std::convert::From<BlockingError<ResponseError>> for ResponseError {
    fn from(err: BlockingError<ResponseError>) -> Self {
        match err {
            BlockingError::Canceled => {
                error!("A task has been cancelled {}", err);
                ResponseError::InternalServerError
            }
            BlockingError::Error(x) => ResponseError::from(x),
        }
    }
}

impl std::convert::From<serde_json::Error> for ResponseError {
    fn from(err: serde_json::Error) -> Self {
        ResponseError::IllFormedRequestSerde(err)
    }
}

impl std::convert::From<visage::redis::RedisError> for ResponseError {
    fn from(err: visage::redis::RedisError) -> Self {
        error!("Error while dealing with redis: {}", err);
        ResponseError::DistantServerError
    }
}
impl std::convert::From<visage::r2d2::Error> for ResponseError {
    fn from(err: visage::r2d2::Error) -> Self {
        error!("R2D2 Error : {}", err);
        ResponseError::ServiceUnavailable
    }
}
