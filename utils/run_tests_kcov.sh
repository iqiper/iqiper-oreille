#!/bin/bash
for test in $(cargo test --no-run --message-format=json | jq -r .executable | grep -ve "^null$")
do
	export COVERAGE_OUT="coverage-$(basename -- $test | cut -d- -f 2)"
	kcov $COVERAGE_OUT $test
done
