use super::*;
use uuid::Uuid;

/// GET /contact
/// Read all contacts
#[iqiper_mains_route(method = "get", route = "/contact")]
async fn route_read_contact_all(
    token: CoreJWTAuth<JWTClaims>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = contact::select_all(&connection, &uid)?;
    Ok(HttpResponse::Ok().json(res))
}

/// GET /contact/{id}
/// Read a contact by its id
#[iqiper_mains_route(method = "get", route = "/contact/{id}")]
async fn route_read_contact_id(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = contact::select_by_id(&connection, &uid, &path.id)?;
    Ok(HttpResponse::Ok().json(res))
}

fn flatten_search_by_username_result(
    uid: &Uuid,
    users: Vec<kcc::keycloak_types::UserRepresentation<'_>>,
) -> Vec<UserSearchResult> {
    users
        .into_iter()
        .filter_map(|user| -> Option<UserSearchResult> {
            let username = user.username?;
            match Uuid::parse_str(user.id?.into_owned().as_str()) {
                Ok(id) => {
                    if id == *uid {
                        return None;
                    }
                    Some(UserSearchResult {
                        id,
                        username: username.into_owned(),
                        first_name: user.first_name.map(|x| x.to_string()),
                        last_name: user.last_name.map(|x| x.to_string()),
                    })
                }
                Err(_x) => None,
            }
        })
        .collect()
}

/// GET /contact/search/{query}
/// Search a contact by username
#[iqiper_mains_route(method = "get", route = "/contact/search/{query}")]
async fn route_search_contact_username(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<UserSearchPath>,
    query: web::Query<UserSearchQuery>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorContact>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(
        &KindContact::SearchUserPath,
        &serde_json::value::to_value(path.as_ref())?,
    )?;
    validator.validate(
        &KindContact::SearchUserQuery,
        &serde_json::value::to_value(query.clone())?,
    )?;
    let kcc = data_server.get_ref().kcc();
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = match kcc
        .search_users_by_username(path.as_ref().query.clone(), query.limit)
        .await
    {
        Ok(users) => Ok(HttpResponse::Ok().json(flatten_search_by_username_result(&uid, users))),
        Err(_err) => Err(ResponseError::DistantServerError),
    };
    res
}
