use super::*;
use crate::{iqiper_oreille_app, validation::Validator};
use peau::actix_rt;
use peau::actix_web::test;

use iqiper_mains::api::OpenAPIOperation;
use iqiper_tealer::oidc::Scope;
use iqiper_tealer::IqiperTealerActor;
use std::collections::HashSet;
use std::iter::FromIterator;

fn get_scope_of_method(op: Option<OpenAPIOperation>) -> Vec<Scope> {
    if op.is_none() {
        return vec![];
    }
    let op = op.unwrap();
    if op.security().is_none() {
        return vec![];
    }
    let mut res: Vec<Scope> = Vec::new();
    for securities in op.security().as_ref().unwrap().iter() {
        for security in securities.values() {
            for scope in security.iter() {
                res.push(Scope::new(scope.clone()));
            }
        }
    }
    return res;
}

#[actix_rt::test]
async fn test_every_scopes() {
    let setup_data = setup().await;
    let user = create_new_test_user(&setup_data, vec![]).await;
    let _oreille = test::init_service(iqiper_oreille_app!(
        setup_data.server_state,
        &setup_data.conf,
        setup_data.conf_mains.clone(),
        setup_data.openapi.clone(),
        crate::setup_metrics().1
    ))
    .await;
    let mut scope_set: HashSet<Scope> = HashSet::new();
    for (path, path_obj) in setup_data.openapi.paths().iter() {
        println!("Mapping {}", path);
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.get().clone()).iter().cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.post().clone()).iter().cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.trace().clone())
                .iter()
                .cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.delete().clone())
                .iter()
                .cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.options().clone())
                .iter()
                .cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.head().clone()).iter().cloned(),
        ));
        scope_set.extend(HashSet::<Scope>::from_iter(
            get_scope_of_method(path_obj.patch().clone())
                .iter()
                .cloned(),
        ));
    }
    let mut tealer_actor = setup_data.tealer.write().expect("The write lock");
    for scope in scope_set.iter() {
        println!("Getting scope {}", scope.to_string());
        let token = tealer_actor
            .get_token_via_password(
                &user.creds.username,
                &user.creds.password,
                &vec![scope.clone()],
            )
            .await
            .expect("The token");
        tealer_actor.tealer_mut().add_token(
            token,
            vec![scope.clone()],
            Some(user.creds.username.to_string()),
        );
    }
    // for (path, path_obj) in setup_data.openapi.paths().iter() {
    //     for method in METHODS.iter() {
    //         if path_obj.operation(&method).is_none() {
    //             continue;
    //         }
    //         let current_scopes = HashSet::<Scope>::from_iter(
    //             get_scope_of_method(path_obj.operation(&method).clone())
    //                 .iter()
    //                 .cloned(),
    //         );
    //         for scope in scope_set.iter() {
    //             let path_built = path.replace("{", "").replace("}", "");
    //             println!(
    //                 "Testing {}#{} with scope {}",
    //                 method.to_uppercase(),
    //                 path_built,
    //                 scope.to_string()
    //             );
    //             let mut req = test::TestRequest::default()
    //                 .method(
    //                     actix_web::http::Method::from_bytes(method.to_uppercase().as_bytes())
    //                         .expect("The method"),
    //                 )
    //                 .uri(path_built.as_str())
    //                 .method(
    //                     actix_web::http::Method::from_bytes(method.to_uppercase().as_bytes())
    //                         .expect("The method"),
    //                 )
    //                 .header("Accept", "application/json")
    //                 .header(
    //                     "Authorization",
    //                     format!(
    //                         "Bearer {}",
    //                         tealer_actor
    //                             .try_get_token_via_password(
    //                                 &user.creds.username,
    //                                 &user.creds.password,
    //                                 &vec![scope.clone()],
    //                             )
    //                             .await
    //                             .expect("The token")
    //                             .token()
    //                             .access_token()
    //                             .secret()
    //                     ),
    //                 )
    //                 .to_request();
    //             println!("{:#?}", req);
    //             let resp = req.send_request(&mut oreille).await;
    //             // println!("{:#?}", actix_web::test::read_body(resp).await);
    //             match current_scopes.contains(scope) {
    //                 true => assert_ne!(
    //                     resp.status(),
    //                     http::StatusCode::UNAUTHORIZED,
    //                     "bad status code returned"
    //                 ),
    //                 false => assert_eq!(
    //                     resp.status(),
    //                     http::StatusCode::UNAUTHORIZED,
    //                     "bad status code returned"
    //                 ),
    //             }
    //         }
    //     }
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.get().clone()).iter().cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.post().clone()).iter().cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.trace().clone())
    //             .iter()
    //             .cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.delete().clone())
    //             .iter()
    //             .cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.options().clone())
    //             .iter()
    //             .cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.head().clone()).iter().cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.patch().clone())
    //             .iter()
    //             .cloned(),
    //     ));
    //     scope_set.extend(HashSet::<Scope>::from_iter(
    //         get_scope_of_method(path_obj.trace().clone())
    //             .iter()
    //             .cloned(),
    //     ));
    // }

    // FIXME For the moment actix crashes when testing middleware that returns an Error
    // TODO Faire une fonction qui fait que l'on peut get un token soit cached, soit refreshed soit un nouveau.
    // TODO Tester toutes les routes. Avec les tokens correspondant, on devrait avoir un resultat different de 401
    // TODO Tester toutes les routes avec les tokens non correspondant, on devrait avoir un resultat = 401
    // let form = notification_crate::TokenApi {
    //     device_id: Uuid::new_v4(),
    //     token: String::from("I am a token"),
    // };
    // let req = test::TestRequest::post()
    //     .uri("/notification")
    //     .set_json(&form)
    //     .header(
    //         "X-Subject",
    //         http::header::HeaderValue::from_str(&format!("\"{}\"", user.id)).expect("invalid uuid"),
    //     )
    //     .header(
    //         "Authorization",
    //         format!("Bearer {}", user.token.token().access_token().secret()),
    //     )
    //     .to_request();
    // let resp = req.send_request(&mut oreille).await;
    // assert_eq!(
    //     resp.status(),
    //     http::StatusCode::OK,
    //     "bad status code returned"
    // );
    // let select: db::fcm_token::GetTokenRecord =
    //     serde_json::from_slice(&test::read_body(resp).await).expect("Return value badly formed");
    // assert_eq!(select.uid, user.id, "uids do not match");
    // assert_eq!(select.device_id, form.device_id, "device_ids do not match");
    // assert_json_snapshot!("token_updated", select, {
    //     ".uid" => dynamic_redaction(|value, _| check_uuid(value, vec![], "[uuid {uid}]")),
    //     ".device_id" => dynamic_redaction(|value, _| check_uuid(value, vec![], "[uuid {device_id}]")),
    //     ".updated_at" => dynamic_redaction(|value, _| check_date(value, "[date {updated_at}]")),
    // });
    delete_test_user(&setup_data, user.id).await;
}
