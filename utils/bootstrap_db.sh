#!/bin/bash
info() { printf "[INFO] (%s) %s\n" "$( date --iso-8601=ns )" "$*" >&2; }

INFRA=$1
URL=$2
TOKEN=$3

info Creating some activities
yq .CreateRequest.example_1.value $INFRA/api/activity/examples/activities.yml | http POST $URL/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_1.value $INFRA/api/activity/examples/activities.yml | http POST $URL/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_1.value $INFRA/api/activity/examples/activities.yml | http POST $URL/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_1.value $INFRA/api/activity/examples/activities.yml | http POST $URL/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
ACTIVITY_ID=$(yq .CreateRequest.example_1.value $INFRA/api/activity/examples/activities.yml | http POST $URL/activity "Authorization:Bearer $TOKEN" | jq -r .id) >/dev/null ; echo .

info Creating some activity templates
yq .Create.example_1.value $INFRA/api/activity_templates/examples/activity_templates.yml | http POST $URL/model/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .Create.example_1.value $INFRA/api/activity_templates/examples/activity_templates.yml | http POST $URL/model/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .Create.example_1.value $INFRA/api/activity_templates/examples/activity_templates.yml | http POST $URL/model/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .Create.example_1.value $INFRA/api/activity_templates/examples/activity_templates.yml | http POST $URL/model/activity "Authorization:Bearer $TOKEN" >/dev/null ; echo .
ACTIVITY_TEMPLATE_ID=$(yq .Create.example_1.value $INFRA/api/activity_templates/examples/activity_templates.yml | http POST $URL/model/activity "Authorization:Bearer $TOKEN" | jq -r .id) >/dev/null ; echo .


info Creating some contacts
yq .CreateRequest.example_2.value $INFRA/api/contacts/examples/contacts.yml | http POST $URL/contact "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_2.value $INFRA/api/contacts/examples/contacts.yml | http POST $URL/contact "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_2.value $INFRA/api/contacts/examples/contacts.yml | http POST $URL/contact "Authorization:Bearer $TOKEN" >/dev/null ; echo .
yq .CreateRequest.example_2.value $INFRA/api/contacts/examples/contacts.yml | http POST $URL/contact "Authorization:Bearer $TOKEN" >/dev/null ; echo .
CONTACT_ID=$(yq .CreateRequest.example_2.value $INFRA/api/contacts/examples/contacts.yml | http POST $URL/contact "Authorization:Bearer $TOKEN" | jq -r .id) >/dev/null ; echo .

info Creating an activity contacts
http POST $URL/activity/$ACTIVITY_ID/contact/$CONTACT_ID "Authorization:Bearer $TOKEN" >/dev/null ; echo .

info Creating an activity template contacts
http POST $URL/model/activity/$ACTIVITY_TEMPLATE_ID/contact/$CONTACT_ID "Authorization:Bearer $TOKEN" >/dev/null ; echo .
