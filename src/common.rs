use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Id {
    pub id: uuid::Uuid,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct IdAndCid {
    pub id: uuid::Uuid,
    pub cid: uuid::Uuid,
}
