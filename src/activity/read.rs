use super::*;

/// GET /activity
/// Read all activities
#[iqiper_mains_route(method = "get", route = "/activity")]
async fn route_read_activity_all(
    token: CoreJWTAuth<JWTClaims>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let selected_activity = db::activity::select_all(&connection, &uid)?;
    Ok(HttpResponse::Ok().json(selected_activity))
}

/// GET /activity/{id}
/// Read all activities id
#[iqiper_mains_route(method = "get", route = "/activity/{id}")]
async fn route_read_activity(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(db::activity::select_by_id(&connection, &uid, &path.id)?))
}

/// GET /activity/{id}/constraint
/// Read all activity constraints
#[iqiper_mains_route(method = "get", route = "/activity/{id}/constraint")]
async fn route_read_activity_constraint_all(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(db::activity_constraint::select_all_by_activity(
            &connection,
            &uid,
            &path.into_inner().id,
        )?),
    )
}

/// GET /activity/{id}/contact
/// Read all activity constraints
#[iqiper_mains_route(method = "get", route = "/activity/{id}/contact")]
async fn route_read_activity_contact_all(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(db::activity_contact::select_contacts_for_activity(
            &connection,
            &uid,
            &path.into_inner().id,
        )?),
    )
}

/// GET /activity/{id}/constraint/{cid}
/// Read an activity constraint
#[iqiper_mains_route(method = "get", route = "/activity/{id}/constraint/{cid}")]
async fn route_read_activity_constraint(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(db::activity_constraint::select_by_id(
            &connection,
            &uid,
            &path.id,
            &path.cid,
        )?),
    )
}
