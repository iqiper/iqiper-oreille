use super::*;

/// POST /notification
/// Create a new notification
#[iqiper_mains_route(method = "post", route = "/notification")]
async fn route_create_notif(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    data_valid: web::Data<ValidatorNotification>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindNotification::Post, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let form: TokenApi = serde_json::from_value(body.0)?;
    let form = fcm_token::NewToken {
        uid,
        device_id: form.device_id,
        token: form.token,
    };
    let res = fcm_token::upsert_fcm_token(&connection, form)?;
    Ok(HttpResponse::Ok().json(res))
}
