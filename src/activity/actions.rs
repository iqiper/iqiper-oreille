use super::*;
use crate::diesel::Connection;

/// Add a communication to the bouche backlog in redis
///
/// # Arguments
/// - `data_server` - The server state
/// - `communication_id` - The id of the communication to handle
///
/// # Return Value
/// Nothing
fn add_to_bouche(
    data_server: &web::Data<ServerState>,
    communication_id: &Uuid,
) -> Result<(), ResponseError> {
    let config = data_server.get_ref().conf().read().map_err(|_e| {
        error!("Configuration shared lock is poisoned");
        ResponseError::InternalServerError
    })?;
    let mut redis_conn = data_server.get_ref().redis().get()?;
    visage::redis::Cmd::lpush(config.bouche_backlog.as_str(), communication_id.to_string())
        .query(&mut *redis_conn)?;
    Ok(())
}

/// Check using the database if a communication should be sent depending on previous communication
///
/// # Arguments
/// - `data_server` - The server state
/// - `uid` - The user id
/// - `communication_id` - The id of the communication to handle
///
/// # Return Value
/// Nothing
fn check_if_should_send_correction(
    conn: &PgConnection,
    uid: &Uuid,
    communication_id: &Uuid,
) -> Result<(), ResponseError> {
    let nb_alert = db::activity_communication::check_if_activity_has_reason(
        // Check that we're not sending a correction
        &conn, // For an alert that was never sent
        &uid,
        &communication_id,
        vec![
            db::enums::CommunicationReasonType::Alert,
            db::enums::CommunicationReasonType::ForcedAlert,
        ],
    )?
    .len();
    if nb_alert == 0 {
        return Err(ResponseError::IllFormedRequestCustom(String::from(
            "Cannot send a correction if no alert was sent",
        )));
    }
    let nb_correction = db::activity_communication::check_if_activity_has_reason(
        // Check that we're not sending a correction
        &conn, // If we already sent one
        &uid,
        &communication_id,
        vec![db::enums::CommunicationReasonType::Correction],
    )?
    .len();
    if nb_correction >= nb_alert {
        return Err(ResponseError::ExpiredDocument);
    }
    Ok(())
}

/// POST /activity/{id}/finish
/// Mark an activity as finished.
#[iqiper_mains_route(method = "post", route = "/activity/{id}/finish")]
async fn route_finish_activity(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let updated_obj = web::block::<_, UpdateResponse, ResponseError>(move || {
        let connection_lock = data_server.get_ref().db_pool_read();
        let conn = connection_lock
            .get()
            .map_err(|_| ResponseError::InternalServerError)?;
        let uid = extract_uid(&conn, &token)?;
        Ok(db::activity::end(&conn, &uid, &path.id)?)
    })
    .await?;
    Ok(HttpResponse::Ok().json(updated_obj))
}

/// POST /activity/{id}/alert
/// Alert all contacts for an activity. It also ends the activity
#[iqiper_mains_route(method = "post", route = "/activity/{id}/alert")]
async fn route_alert_activity(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communication_obj =
        web::block::<_, db::communication::SelectCommunication, ResponseError>(move || {
            let connection_lock = data_server.get_ref().db_pool_read();
            let conn = connection_lock
                .get()
                .map_err(|_| ResponseError::InternalServerError)?;
            let uid = extract_uid(&conn, &token)?;
            let res = conn
                .transaction::<db::communication::SelectCommunication, ResponseError, _>(|| {
                    match activity::end(&conn, &uid, &path.id) {
                        Ok(_x) => (),
                        Err(db::error::IqiperDatabaseError::ReadOnly) => (),
                        Err(db::error::IqiperDatabaseError::DatabaseError(
                            diesel::result::Error::NotFound,
                        )) => (),
                        Err(x) => return Err(ResponseError::from(x)),
                    };
                    let communication = db::communication::create_from_activity(
                        &conn,
                        &path.id,
                        db::communication::NewCommunication {
                            uid,
                            type_: db::enums::CommunicationType::Email,
                            reason: db::enums::CommunicationReasonType::ForcedAlert,
                        },
                    )?;
                    let communication_obj =
                        db::communication::select_by_id(&conn, &uid, &communication.id)?;
                    Ok(communication_obj)
                })?;
            add_to_bouche(&data_server, &res.id)?;
            Ok(res)
        })
        .await?;
    Ok(HttpResponse::Ok().json(communication_obj))
}

/// POST /activity/{id}/alert
/// Alert all contacts for an activity. It also ends the activity
#[iqiper_mains_route(method = "post", route = "/activity/{id}/alert/correct")]
async fn route_correct_activity_alert(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let communication_obj =
        web::block::<_, db::communication::SelectCommunication, ResponseError>(move || {
            let connection_lock = data_server.get_ref().db_pool_read();
            let conn = connection_lock
                .get()
                .map_err(|_| ResponseError::InternalServerError)?;
            let uid = extract_uid(&conn, &token)?;
            let res = conn
                .transaction::<db::communication::SelectCommunication, ResponseError, _>(|| {
                    match activity::end(&conn, &uid, &path.id) {
                        Ok(_x) => (),
                        Err(db::error::IqiperDatabaseError::ReadOnly) => (),
                        Err(db::error::IqiperDatabaseError::DatabaseError(
                            diesel::result::Error::NotFound,
                        )) => (),
                        Err(x) => return Err(ResponseError::from(x)),
                    };
                    check_if_should_send_correction(&conn, &uid, &path.id)?;
                    let communication = db::communication::create_from_activity(
                        &conn,
                        &path.id,
                        db::communication::NewCommunication {
                            uid,
                            type_: db::enums::CommunicationType::Email,
                            reason: db::enums::CommunicationReasonType::Correction,
                        },
                    )?;
                    let communication_obj =
                        db::communication::select_by_id(&conn, &uid, &communication.id)?;
                    Ok(communication_obj)
                })?;
            add_to_bouche(&data_server, &res.id)?;
            Ok(res)
        })
        .await?;
    Ok(HttpResponse::Ok().json(communication_obj))
}
