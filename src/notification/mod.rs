mod create;
mod delete;

use iqiper_mains;
use iqiper_mains::iqiper_mains_route;

use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::users_utils::extract_uid;
use crate::validation::{SchemaKind, Validator};
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};

use db::fcm_token;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

impl SchemaKind for KindNotification {
    fn lst_entries() -> Vec<(Self, String, String)> {
        vec![
            (
                KindNotification::Post,
                format!("{}", "fcm_token/post.json"),
                include_str!("../../schemas/fcm_token/post.json").to_string(),
            ),
            (
                KindNotification::Delete,
                format!("{}", "fcm_token/delete.json"),
                include_str!("../../schemas/fcm_token/delete.json").to_string(),
            ),
        ]
    }
}

impl Validator for ValidatorNotification {
    type Kind = KindNotification;

    fn get_map(&self) -> &HashMap<Self::Kind, Value> {
        &self.schemas_values
    }
    fn get_map_mut(&mut self) -> &mut HashMap<Self::Kind, Value> {
        &mut self.schemas_values
    }
}

impl ValidatorNotification {
    pub fn new() -> Self {
        ValidatorNotification {
            schemas_values: HashMap::with_capacity(2),
        }
    }
}
impl ValidatorNotification {}

#[derive(Deserialize, Serialize)]
pub struct TokenApi {
    pub device_id: uuid::Uuid,
    pub token: String,
}

#[derive(Deserialize, Serialize)]
pub struct TokenDevice {
    pub device_id: uuid::Uuid,
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum KindNotification {
    Post,
    Delete,
}

#[derive(Clone)]
pub struct ValidatorNotification {
    schemas_values: HashMap<KindNotification, Value>,
}

/// Set the route for the notification feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(create::route_create_notif)
        .service(delete::route_delete_notif);
}
