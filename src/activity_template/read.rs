use super::*;

/// GET /model/activity
/// Read all activities
#[iqiper_mains_route(method = "get", route = "/model/activity")]
async fn route_read_activity_template_all(
    token: CoreJWTAuth<JWTClaims>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(db::activity_template::select_all(&connection, &uid)?))
}

/// GET /model/activity/{id}
/// Read all activities id
#[iqiper_mains_route(method = "get", route = "/model/activity/{id}")]
async fn route_read_activity_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(db::activity_template::select_by_id(
        &connection,
        &uid,
        &path.id,
    )?))
}

/// GET /model/activity/{id}/contact
/// Read all activity contacts
#[iqiper_mains_route(method = "get", route = "/model/activity/{id}/contact")]
async fn route_read_activity_contact_template_all(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(
        db::activity_contact_template::select_contacts_for_activity_template(
            &connection,
            &uid,
            &path.into_inner().id,
        )?,
    ))
}

/// GET /model/activity/{id}/constraint
/// Read all activity constraints
#[iqiper_mains_route(method = "get", route = "/model/activity/{id}/constraint")]
async fn route_read_activity_constraint_template_all(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(db::activity_constraint_template::select_all_by_activity(
            &connection,
            &uid,
            &path.into_inner().id,
        )?),
    )
}

/// GET /model/activity/{id}/constraint/{cid}
/// Read an activity constraint
#[iqiper_mains_route(method = "get", route = "/model/activity/{id}/constraint/{cid}")]
async fn route_read_activity_constraint_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(db::activity_constraint_template::select_by_id(
            &connection,
            &uid,
            &path.id,
            &path.cid,
        )?),
    )
}
