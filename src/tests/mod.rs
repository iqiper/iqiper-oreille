use iqiper_tealer::oidc::OAuth2TokenResponse;

#[macro_export]
macro_rules! oreille_exec {
    ($oreille:expr, $user:expr, $method:ident, $path:expr, $expect_status_code:expr) => {{
        let req = test::TestRequest::$method()
            .uri($path)
            .header(
                "X-Subject",
                http::header::HeaderValue::from_str(&format!("\"{}\"", $user.id))
                    .expect("invalid uuid"),
            )
            .header(
                "Authorization",
                format!("Bearer {}", $user.token.token().access_token().secret()),
            );
        let resp = req.send_request(&mut $oreille).await;
        assert_eq!(
            resp.status(),
            $expect_status_code,
            "bad status code returned"
        );
        let test_result: serde_json::Value = serde_json::from_slice(&test::read_body(resp).await)
            .expect("Return value badly formed");
        test_result
    }};
    ($oreille:expr, $user:expr, $method:ident, $path:expr, $expect_status_code:expr, $payload:expr) => {{
        let req = test::TestRequest::$method()
            .uri($path)
            .set_json(&$payload)
            .header(
                "X-Subject",
                http::header::HeaderValue::from_str(&format!("\"{}\"", $user.id))
                    .expect("invalid uuid"),
            )
            .header(
                "Authorization",
                format!("Bearer {}", $user.token.token().access_token().secret()),
            );
        let resp = req.send_request(&mut $oreille).await;
        assert_eq!(
            resp.status(),
            $expect_status_code,
            "bad status code returned"
        );
        let test_result: serde_json::Value = serde_json::from_slice(&test::read_body(resp).await)
            .expect("Return value badly formed");
        test_result
    }};
}
mod activity;
mod scopes;
use crate::data_state::configuration::{self, Configuration};
use crate::data_state::server_state::ServerState;
use crate::CONF_FILE;
use db::distant_user;
use iqiper_mains::jwt_extractor::CoreJWTConfig;
use iqiper_tealer::oidc::{
    ClientId, ClientSecret, IssuerUrl, ResourceOwnerPassword, ResourceOwnerUsername, Scope,
};
use iqiper_tealer::{
    CoreIqiperTealerActor, IqiperTealer, IqiperTealerActorPasswordGrant, IqiperTealerToken,
};
use peau::actix_web::http;
const NB_REDIS_DB: u8 = 16;
use async_std::task;
use once_cell::sync::Lazy;
use std::sync::{Arc, Mutex, MutexGuard, RwLock};
use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct TestUser {
    username: ResourceOwnerUsername,
    password: ResourceOwnerPassword,
}

#[derive(Clone, Debug)]
pub struct TestUserAndCreds {
    pub id: Uuid,
    pub creds: TestUser,
    pub token: IqiperTealerToken,
}

pub static CONF: Lazy<configuration::Configuration> = Lazy::new(|| {
    configuration::get(std::path::Path::new(
        &std::env::var(CONF_FILE).expect("The configuration path environment variable"),
    ))
    .expect("the configuration")
});

pub async fn delete_test_user<'a>(setup_data: &OreilleTestSetup<'a>, test_user: Uuid) {
    distant_user::delete_user(&setup_data.conn, &test_user).expect("User deletion failed");
    setup_data
        .server_state
        .kcc()
        .destroy_user(test_user)
        .await
        .expect("To have destroyed the user on keycloak");
}

pub async fn create_new_test_user(
    setup_data: &'_ OreilleTestSetup<'_>,
    scope: Vec<&str>,
) -> TestUserAndCreds {
    let scope: Vec<Scope> = scope
        .into_iter()
        .map(|x| Scope::new(x.to_string()))
        .collect();
    let test_user_data = TestUser {
        username: ResourceOwnerUsername::new(Uuid::new_v4().to_string()),
        password: ResourceOwnerPassword::new(Uuid::new_v4().to_string()),
    };
    let user_id = setup_data
        .server_state
        .kcc()
        .create_user(kcc::keycloak_types::UserRepresentation {
            first_name: Some(std::borrow::Cow::Borrowed(
                Uuid::new_v4().to_string().as_str(),
            )),
            last_name: Some(std::borrow::Cow::Borrowed(
                Uuid::new_v4().to_string().as_str(),
            )),
            username: Some(std::borrow::Cow::Borrowed(test_user_data.username.as_str())),
            email: Some(std::borrow::Cow::Borrowed(
                format!("{}@test.local", test_user_data.username.as_str()).as_str(),
            )),
            email_verified: Some(true),
            enabled: Some(true),
            credentials: Some(vec![kcc::keycloak_types::CredentialRepresentation {
                type_: Some(std::borrow::Cow::Borrowed("password")),
                temporary: Some(false),
                value: Some(std::borrow::Cow::Borrowed(
                    test_user_data.password.secret().as_str(),
                )),
                ..Default::default()
            }]),
            ..Default::default()
        })
        .await
        .expect("To create the user in keycloak");
    distant_user::create_user(&setup_data.conn, &user_id).expect("user creation failed");
    let tealer = setup_data.tealer.read().expect("The read lock");
    let token = tealer
        .get_token_via_password(&test_user_data.username, &test_user_data.password, &scope)
        .await
        .expect("The token");
    TestUserAndCreds {
        id: user_id,
        creds: test_user_data,
        token,
    }
}

pub struct OreilleTestSetup<'a> {
    pub conf: Configuration,
    pub server_state: ServerState,
    pub conn: r2d2::PooledConnection<diesel::r2d2::ConnectionManager<diesel::PgConnection>>,
    pub conf_mains: iqiper_mains::AuthConfig<'a>,
    pub tealer: Arc<RwLock<CoreIqiperTealerActor>>,
    pub openapi: iqiper_mains::api::OpenAPIDocument,
}

pub async fn setup<'a>() -> OreilleTestSetup<'a> {
    let conf_path = std::env::var(CONF_FILE).expect("The configuration path environment variable");
    let mut conf = configuration::get(std::path::Path::new(&conf_path)).expect("the configuration");
    let db = lock_db().await;
    conf.redis.database = Some((*db).to_string());
    let server_state = ServerState::new(conf.clone())
        .await
        .expect("Test failed to create server state");
    let conn_lock = server_state.db_pool_read();
    let conn = conn_lock.get().expect("To get a connection");
    let settings_mains =
        configuration::get_mains(&conf, &server_state.kcc()).expect("iqiper-mains configuration");
    let mut jwks = iqiper_mains::request_jwks(
        server_state.keycloak_client(),
        &settings_mains.idp.jwks_route,
    )
    .await;
    for i in 1..100
    // Retry to get the jwks, keycloak can get overwhelmed during heavy testings
    {
        if jwks.keys.len() > 0 {
            break;
        }
        std::thread::sleep(std::time::Duration::from_millis(i * 10));
        jwks = iqiper_mains::request_jwks(
            server_state.keycloak_client(),
            &settings_mains.idp.jwks_route,
        )
        .await;
    }
    assert_ne!(jwks.keys.len(), 0, "The JWKS shouldn't be empty");
    let conf_mains =
        iqiper_mains::AuthConfig::new(settings_mains, jwks, server_state.keycloak_client().clone());
    let openapi_specs =
        iqiper_mains::api::read_api_file(conf.openapi_specs.as_str()).expect("The openapi specs");
    let tealer = CoreIqiperTealerActor::new(
        IqiperTealer::new_direct_grant(
            peau::reqwest::Client::new(),
            IssuerUrl::new(format!(
                "{}://{}:{}/auth/realms/{}",
                CONF.keycloak.http.scheme,
                CONF.keycloak.http.host,
                CONF.keycloak.http.port,
                CONF.keycloak.realm
            ))
            .expect("Failed to parse the IdP discovery base URL"),
            ClientId::new(CONF.keycloak.client_id.clone()),
            Some(ClientSecret::new(CONF.keycloak.client_secret.clone())),
        )
        .await
        .expect("To discover correctly"),
    );
    let mut res = OreilleTestSetup {
        conf: conf.clone(),
        server_state: ServerState::new(conf)
            .await
            .expect("Test failed to create server state"),
        conn,
        conf_mains,
        tealer: Arc::new(RwLock::new(tealer)),
        openapi: openapi_specs,
    };
    clean_up_redis(&mut res);
    res
}

static DB_POOL: Lazy<Vec<Mutex<u8>>> = Lazy::new(|| {
    let mut res: Vec<Mutex<u8>> = vec![];
    for i in 0..NB_REDIS_DB {
        res.push(Mutex::new(i));
    }
    res
});

pub async fn lock_db() -> MutexGuard<'static, u8> {
    loop {
        for db in DB_POOL.iter() {
            let tmp = db.try_lock();
            if tmp.is_ok() {
                return tmp.unwrap();
            }
        }
        task::sleep(std::time::Duration::from_millis(200)).await;
    }
}

pub fn clean_up_redis(setup: &mut OreilleTestSetup) {
    let res: bool = visage::redis::cmd("FLUSHDB")
        .query(
            &mut *setup
                .server_state
                .redis()
                .get()
                .expect("to get a redis connection"),
        )
        .expect("Flushed fails");
    assert_eq!(res, true, "Clean up went wrong");
}
