mod create;
mod delete;
mod read;
mod update;

use crate::common::*;
use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::users_utils::extract_uid;
use crate::validation::{SchemaKind, Validator};
use iqiper_mains;
use iqiper_mains::iqiper_mains_route;
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};

use db::contact;

use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

impl SchemaKind for KindContact {
    fn lst_entries() -> Vec<(Self, String, String)> {
        vec![
            (
                KindContact::Create,
                format!("{}", "contact/create.json"),
                include_str!("../../schemas/contact/create.json").to_string(),
            ),
            (
                KindContact::UpdateFavorite,
                format!("{}", "contact/update_favorite.json"),
                include_str!("../../schemas/contact/update_favorite.json").to_string(),
            ),
            (
                KindContact::UpdateNickname,
                format!("{}", "contact/update_nickname.json"),
                include_str!("../../schemas/contact/update_nickname.json").to_string(),
            ),
            (
                KindContact::SearchUserPath,
                format!("{}", "contact/search_by_username_path.json"),
                include_str!("../../schemas/contact/search_by_username_path.json").to_string(),
            ),
            (
                KindContact::SearchUserQuery,
                format!("{}", "contact/search_by_username_query.json"),
                include_str!("../../schemas/contact/search_by_username_query.json").to_string(),
            ),
        ]
    }
}

impl Validator for ValidatorContact {
    type Kind = KindContact;

    fn get_map(&self) -> &HashMap<Self::Kind, Value> {
        &self.schemas_values
    }
    fn get_map_mut(&mut self) -> &mut HashMap<Self::Kind, Value> {
        &mut self.schemas_values
    }
}

impl ValidatorContact {
    pub fn new() -> Self {
        ValidatorContact {
            schemas_values: HashMap::with_capacity(5),
        }
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserSearchPath {
    pub query: String,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserSearchQuery {
    pub limit: Option<u32>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UserSearchResult {
    pub id: uuid::Uuid,
    pub username: String,
    pub first_name: Option<String>,
    pub last_name: Option<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct UpdateNickname {
    pub nickname: String,
}

#[derive(Deserialize, Serialize)]
pub struct UpdateFavorite {
    pub favorite: bool,
}

#[derive(Deserialize, Serialize)]
pub struct UpdateResponse {
    pub id: uuid::Uuid,
    pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum KindContact {
    Create,
    UpdateFavorite,
    UpdateNickname,
    SearchUserPath,
    SearchUserQuery,
}

#[derive(Clone)]
pub struct ValidatorContact {
    schemas_values: HashMap<KindContact, Value>,
}

/// Set the route for the contact feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(create::route_create_contact)
        .service(read::route_read_contact_all)
        .service(read::route_read_contact_id)
        .service(read::route_search_contact_username)
        .service(update::route_update_contact_favorite)
        .service(update::route_update_contact_nickname)
        .service(delete::route_delete_contact_by_id);
}
