mod actions;
mod create;
mod delete;
mod read;
mod update;

use crate::common::*;
use iqiper_mains;
use iqiper_mains::iqiper_mains_route;

use crate::data_state::server_state::ServerState;
use crate::error::ResponseError;
use crate::validation::{SchemaKind, Validator};
use peau::actix_web;
use peau::actix_web::{web, HttpResponse};

use crate::users_utils::extract_uid;
use iqiper_mains::jwt_extractor::CoreJWTAuth;
use iqiper_mains::JWTClaims;

use chrono::{DateTime, Utc};
use db::activity::{self, NewActivity, NewActivityWithoutUid};
use db::activity_constraint;
use db::activity_contact::{self, ActivityContact};
use db::common::{IdTsResponse, UpdateResponse};

use db::enums::ActivityConstraintType;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::collections::HashMap;

use uuid::Uuid;
#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(tag = "type")]
#[serde(rename_all = "snake_case")]
pub enum ConstraintCreator {
    Timer(CreateConstraintTimer),
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
pub struct CreateConstraintTimer {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub alert_message: Option<String>,
    pub start_date: DateTime<Utc>,
    pub end_date: DateTime<Utc>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub notify_date: Option<DateTime<Utc>>,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateName {
    pub name: String,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateAlertMessage {
    pub alert_message: Option<String>,
}

#[derive(PartialEq, Clone, Debug, Deserialize, Serialize)]
pub struct UpdateIcon {
    pub icon: Option<i32>,
}

#[derive(PartialEq, Eq, Hash, Clone)]
pub enum KindActivity {
    Create,
    UpdateName,
    UpdateAlertMessage,
    UpdateIcon,
    CreateConstraint,
}

impl SchemaKind for KindActivity {
    fn lst_entries() -> Vec<(Self, String, String)> {
        vec![
            (
                KindActivity::Create,
                format!("{}", "activity/create.json"),
                include_str!("../../schemas/activity/create.json").to_string(),
            ),
            (
                KindActivity::UpdateName,
                format!("{}", "activity/update_name.json"),
                include_str!("../../schemas/activity/update_name.json").to_string(),
            ),
            (
                KindActivity::UpdateAlertMessage,
                format!("{}", "activity/update_alert_message.json"),
                include_str!("../../schemas/activity/update_alert_message.json").to_string(),
            ),
            (
                KindActivity::UpdateIcon,
                format!("{}", "activity/update_icon.json"),
                include_str!("../../schemas/activity/update_icon.json").to_string(),
            ),
            (
                KindActivity::CreateConstraint,
                format!("{}", "activity/create_constraint.json"),
                include_str!("../../schemas/activity/create_constraint.json").to_string(),
            ),
        ]
    }
}

#[derive(Clone)]
pub struct ValidatorActivity {
    schemas_values: HashMap<KindActivity, Value>,
}

impl Validator for ValidatorActivity {
    type Kind = KindActivity;

    fn get_map(&self) -> &HashMap<Self::Kind, Value> {
        &self.schemas_values
    }
    fn get_map_mut(&mut self) -> &mut HashMap<Self::Kind, Value> {
        &mut self.schemas_values
    }
}

impl ValidatorActivity {
    pub fn new() -> Self {
        ValidatorActivity {
            schemas_values: HashMap::with_capacity(3),
        }
    }
}

/// Set the route for the activity feature
///
/// # Arguments
/// *`cfg` - The service configuration
pub fn set_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(create::route_create_activity)
        .service(create::route_create_activity_contact)
        .service(create::route_create_activity_constraint)
        .service(read::route_read_activity_all)
        .service(read::route_read_activity)
        .service(read::route_read_activity_contact_all)
        .service(read::route_read_activity_constraint_all)
        .service(read::route_read_activity_constraint)
        .service(actions::route_finish_activity)
        .service(actions::route_alert_activity)
        .service(actions::route_correct_activity_alert)
        .service(update::route_update_activity_message)
        .service(update::route_update_activity_icon)
        .service(update::route_update_activity_name)
        .service(update::route_update_activity_constraint)
        .service(delete::route_delete_activity)
        .service(delete::route_delete_activity_contact)
        .service(delete::route_delete_activity_constraint);
}
