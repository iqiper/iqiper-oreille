use super::*;

/// DELETE /self/destroy
/// Delete oneself
#[iqiper_mains_route(method = "delete", route = "/self/destroy")]
async fn route_delete_self(
    token: CoreJWTAuth<JWTClaims>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let _number_of_deleted = web::block::<_, usize, db::error::IqiperDatabaseError>(move || {
        Ok(db::distant_user::delete_user(&connection, &uid)?)
    })
    .await?;
    data_server
        .as_ref()
        .kcc()
        .destroy_user(uid)
        .await
        .map_err(|err| {
            error!("Error while deleting an user from Keycloak : {}", err);
            ResponseError::ServiceUnavailable
        })?;
    Ok(HttpResponse::Ok().finish())
}
