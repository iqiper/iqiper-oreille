use super::*;

/// PUT /contact/{id}/nickname
/// Change a contact nickname
#[iqiper_mains_route(method = "put", route = "/contact/{id}/nickname")]
async fn route_update_contact_nickname(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<Id>,
    data_valid: web::Data<ValidatorContact>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindContact::UpdateNickname, &body)?;
    let form = serde_json::from_value::<UpdateNickname>(body.0)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = contact::update_nickname(&connection, &uid, &path.id, form.nickname);
    let res = res?;
    Ok(HttpResponse::Ok().json(UpdateResponse {
        id: res.0,
        updated_at: res.1,
    }))
}

/// PUT /contact/{id}/favorite
/// Change a contact favorite status
#[iqiper_mains_route(method = "put", route = "/contact/{id}/favorite")]
async fn route_update_contact_favorite(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<Id>,
    data_valid: web::Data<ValidatorContact>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindContact::UpdateFavorite, &body)?;
    let form = serde_json::from_value::<UpdateFavorite>(body.0)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let res = contact::update_favorite(&connection, &uid, &path.id, form.favorite)?;
    Ok(HttpResponse::Ok().json(UpdateResponse {
        id: res.0,
        updated_at: res.1,
    }))
}
