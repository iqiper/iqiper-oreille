#!/bin/bash
docker run -v $PWD:/volume -v ${SSH_AUTH_SOCK}:${SSH_AUTH_SOCK} -e SSH_AUTH_SOCK="${SSH_AUTH_SOCK}" --rm -t clux/muslrust cargo build --release
