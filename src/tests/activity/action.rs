use super::*;
use crate::{iqiper_oreille_app, validation::Validator};
use peau::actix_rt;
use peau::actix_web::test;
use std::str::FromStr;
use test_db::*;
use visage::redis;

async fn local_setup<'a>() -> (OreilleTestSetup<'a>, TestUserAndCreds, Uuid) {
    let setup_data = setup::<'a>().await;
    let user = create_new_test_user(&setup_data, vec!["iqiper.io/activity"]).await;
    let activity = create_activity(&setup_data.conn, &user.id, true, None, None);
    let contact = create_contact(&setup_data.conn, &user.id);
    let _activity_contact =
        create_activity_contact(&setup_data.conn, &user.id, &activity, &contact);
    (setup_data, user, activity)
}

fn check_redis(state: &OreilleTestSetup<'_>) -> Option<Uuid> {
    let mut conn = state
        .server_state
        .redis()
        .get()
        .expect("to get a redis connection");
    let res: Option<Uuid> = redis::Cmd::lpop(state.conf.bouche_backlog.as_str())
        .query::<Option<String>>(&mut *conn)
        .expect("Should have fetch a communication id")
        .map(|x| Uuid::from_str(x.as_str()).expect("should be a valid uuid"));
    res
}

#[actix_rt::test]
async fn alert_activity() {
    let (setup_data, user, activity) = local_setup().await;
    let mut oreille = test::init_service(iqiper_oreille_app!(
        setup_data.server_state,
        &setup_data.conf,
        setup_data.conf_mains.clone(),
        setup_data.openapi.clone(),
        crate::setup_metrics().1
    ))
    .await;
    let res = oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/alert", activity).as_str(),
        200
    );
    let res: db::communication::SelectCommunication =
        serde_json::from_value(res).expect("to deserialize the object");
    let id = check_redis(&setup_data);
    assert_eq!(id.is_some(), true, "there should be a communication id");
    assert_eq!(id.unwrap(), res.id, "Ids mismatch");
    let id = check_redis(&setup_data);
    assert_eq!(
        id.is_none(),
        true,
        "there shouldn't be a second communication id"
    );
    super::super::delete_test_user(&setup_data, user.id).await;
}

#[actix_rt::test]
async fn alert_activity_ended() {
    let (setup_data, user, activity) = local_setup().await;
    let mut oreille = test::init_service(iqiper_oreille_app!(
        setup_data.server_state,
        &setup_data.conf,
        setup_data.conf_mains.clone(),
        setup_data.openapi.clone(),
        crate::setup_metrics().1
    ))
    .await;
    let res = oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/finish", activity).as_str(),
        200
    );
    let res: db::common::UpdateResponse =
        serde_json::from_value(res).expect("to deserialize the object");
    assert_eq!(res.id, activity, "should've recieved the activity");
    let res = oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/alert", activity).as_str(),
        200
    );
    let res: db::communication::SelectCommunication =
        serde_json::from_value(res).expect("to deserialize the object");
    let id = check_redis(&setup_data);
    assert_eq!(id.is_some(), true, "there should be a communication id");
    assert_eq!(id.unwrap(), res.id, "Ids mismatch");
    let id = check_redis(&setup_data);
    assert_eq!(
        id.is_none(),
        true,
        "there shouldn't be a second communication id"
    );
    super::super::delete_test_user(&setup_data, user.id).await;
}

#[actix_rt::test]
async fn correct_activity() {
    let (setup_data, user, activity) = local_setup().await;
    let mut oreille = test::init_service(iqiper_oreille_app!(
        setup_data.server_state,
        &setup_data.conf,
        setup_data.conf_mains.clone(),
        setup_data.openapi.clone(),
        crate::setup_metrics().1
    ))
    .await;
    // Alert
    let res = oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/alert", activity).as_str(),
        200
    );
    let res: db::communication::SelectCommunication =
        serde_json::from_value(res).expect("to deserialize the object");
    let id = check_redis(&setup_data);
    assert_eq!(id.is_some(), true, "there should be a communication id");
    assert_eq!(id.unwrap(), res.id, "Ids mismatch");
    let id = check_redis(&setup_data);
    assert_eq!(
        id.is_none(),
        true,
        "there shouldn't be a second communication id"
    );
    // Correction
    let res = oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/alert/correct", activity).as_str(),
        200
    );
    let res: db::communication::SelectCommunication =
        serde_json::from_value(res).expect("to deserialize the object");
    let id = check_redis(&setup_data);
    assert_eq!(id.is_some(), true, "there should be a communication id");
    assert_eq!(id.unwrap(), res.id, "Ids mismatch");
    let id = check_redis(&setup_data);
    assert_eq!(
        id.is_none(),
        true,
        "there shouldn't be a second communication id"
    );
    super::super::delete_test_user(&setup_data, user.id).await;
}

#[actix_rt::test]
async fn correct_activity_without_alert() {
    let (setup_data, user, activity) = local_setup().await;
    let mut oreille = test::init_service(iqiper_oreille_app!(
        setup_data.server_state,
        &setup_data.conf,
        setup_data.conf_mains.clone(),
        setup_data.openapi.clone(),
        crate::setup_metrics().1
    ))
    .await;
    // Alert
    oreille_exec!(
        oreille,
        user,
        post,
        format!("/activity/{}/alert/correct", activity).as_str(),
        400
    );
    super::super::delete_test_user(&setup_data, user.id).await;
}
