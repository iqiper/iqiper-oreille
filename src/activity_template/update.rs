use super::*;

/// Update an activity constraint
///
/// # Arguments
/// - `conn` - The connection to the database
/// - `uid` - The user id
/// - `activity_template_id` - The activity template id
/// - `constraint_template_id` - The activity constraint template id
/// - `constraint` - The constraint object to create
///
/// # Return Value
/// A result to either the UpdateResponse or an database error
pub fn update_activity_constraint_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    constraint_template_id: &Uuid,
    constraint: ConstraintCreator,
) -> Result<UpdateResponse, db::error::IqiperDatabaseError> {
    match constraint {
        ConstraintCreator::Timer(x) => activity_constraint_template::modify(
            conn,
            uid,
            activity_template_id,
            constraint_template_id,
            activity_constraint_template::NewActivityConstraintTemplate {
                type_: ActivityConstraintTemplateType::Timer,
                activity_template_id: *activity_template_id,
                start_offset: x.start_offset,
                end_offset: x.end_offset,
                notify_offset: x.notify_offset,
                alert_message: x.alert_message,
            },
        ),
    }
}

/// PUT /model/activity/{id}/name
/// Change an activity name
#[iqiper_mains_route(method = "put", route = "/model/activity/{id}/name")]
async fn route_update_activity_template_name(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivityTemplate>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::UpdateName, &body)?;
    let body = UpdateName::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let tmp =
        db::activity_template::update_name(&connection, &uid, &path.into_inner().id, body.name)?;
    Ok(HttpResponse::Ok().json(tmp))
}

/// PUT /model/activity/{id}/message
/// Change an activity message
#[iqiper_mains_route(method = "put", route = "/model/activity/{id}/alert_message")]
async fn route_update_activity_template_message(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivityTemplate>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::UpdateMessage, &body)?;
    let body = UpdateMessage::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let tmp = db::activity_template::update_alert_message(
        &connection,
        &uid,
        &path.into_inner().id,
        body.alert_message,
    )?;
    Ok(HttpResponse::Ok().json(tmp))
}

/// PUT /model/activity/{id}/icon
/// Change an activity icon
#[iqiper_mains_route(method = "put", route = "/model/activity/{id}/icon")]
async fn route_update_activity_template_icon(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<Id>,
    body: web::Json<Value>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivityTemplate>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::UpdateIcon, &body)?;
    let body = UpdateIcon::deserialize(body.into_inner())?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(db::activity_template::update_icon(
        &connection,
        &uid,
        &path.into_inner().id,
        body.icon,
    )?))
}

/// PUT /model/activity/{id}/constraint/{cid}
/// Modify an activity constraint
#[iqiper_mains_route(method = "put", route = "/model/activity/{id}/constraint/{cid}")]
async fn route_update_activity_constraint_template(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivityTemplate>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::CreateConstraint, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(update_activity_constraint_template(
        &connection,
        &uid,
        &path.id,
        &path.cid,
        ConstraintCreator::deserialize(body.into_inner())?,
    )?))
}
