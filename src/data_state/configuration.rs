use iqiper_tealer::IqiperTealerActor;
use kcc::KeycloakClient;
use peau::configuration::ConfigurationServer;
use serde::Deserialize;
use strum_macros::EnumString;

#[derive(Clone, Debug, PartialEq, EnumString, strum_macros::ToString, Deserialize)]
#[strum(serialize_all = "kebab-case")]
#[serde(rename_all(deserialize = "kebab-case"))]
pub enum SSLModeType {
    Disable,
    Allow,
    Prefer,
    Require,
    VerifyCa,
    VerifyFull,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationDatabase {
    pub host: String,
    pub port: u32,
    pub user: String,
    pub password: String,
    pub database: String,
    pub connection_number: String,
    pub run_migrations: bool,
    pub quit_after_migrations: Option<bool>,
    pub sslmode: Option<SSLModeType>,
    pub sslcert: Option<String>,
    pub sslkey: Option<String>,
    pub sslrootcert: Option<String>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationKeycloak {
    #[serde(flatten)]
    pub http: peau::configuration::ConfigurationServerDistant,
    pub realm: String,
    pub client_id: String,
    pub client_secret: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ConfigurationFromFile {
    pub database: ConfigurationDatabase,
    pub server_public: ConfigurationServer,
    pub server_private: ConfigurationServer,
    pub schemas: String,
    pub keycloak: ConfigurationKeycloak,
    pub bouche: peau::configuration::ConfigurationServerDistant,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Configuration {
    pub database: ConfigurationDatabase,
    pub server_public: ConfigurationServer,
    pub server_private: ConfigurationServer,
    pub keycloak: ConfigurationKeycloak,
    pub jwt: iqiper_mains::JwtSettings,
    pub redis: visage::configuration::ConfigurationRedis,
    pub bouche_backlog: String,
    pub openapi_specs: String,
}

/// Get the configuration from the path specified
///
/// # Arguments
/// - `path` - The configuration path
///
/// # Return value
/// The configuration object
pub fn get(path: &std::path::Path) -> Result<Configuration, config::ConfigError> {
    let mut settings = config::Config::default();
    settings
        .merge(config::File::from(path))?
        .merge(config::Environment::with_prefix("IQIPER_OREILLE").separator("__"))?;
    Ok(settings.try_into::<Configuration>()?)
}

/// Get the configuration for `iqiper-mains` from `iqiper-oreille`
/// configuration and a Keycloak connection
///
/// # Arguments
/// - `config` - The `iqiper-oreille` configuration
/// - `kcc` - The Keycloak Client
///
/// # Return value
/// The configuration object
pub fn get_mains(
    config: &Configuration,
    kcc: &KeycloakClient,
) -> Result<iqiper_mains::AuthSettings, config::ConfigError> {
    let tealer_actor_lock = kcc.tealer_actor().clone();
    let tealer_actor = tealer_actor_lock.read().map_err(|e| {
        config::ConfigError::Message(format!("Can't lock the tealer metadata object : {}", e))
    })?;
    let oidc_metadata = tealer_actor.tealer().oidc_metadata();
    let mut settings = config::Config::try_from::<iqiper_mains::AuthSettings>(&iqiper_mains::AuthSettings {
    	idp: iqiper_mains::IdpSettings {
    		client_id: config.keycloak.client_id.clone(),
    		client_secret: config.keycloak.client_secret.clone(),
    		token_route: oidc_metadata.token_endpoint().ok_or_else(|| config::ConfigError::Message(String::from("No token endpoint provided when building `iqiper-mains` configuration.")))?.to_string(),
    		jwks_route: oidc_metadata.jwks_uri().to_string(),
    	},
    	jwt: config.jwt.clone(),
    	local_server: iqiper_mains::LocalServerSettings {
			host: config.server_public.host.clone(),
			port: config.server_public.port,
			advertise_uri: config.server_public.advertise_uri.clone(),
			advertise_scheme: config.server_public.advertise_scheme.clone(),
			local_code_route: String::from("/auth/token"), //TODO Better
		},
	})?;
    settings.merge(config::Environment::with_prefix("IQIPER_MAINS"))?;
    let res = settings.try_into::<iqiper_mains::AuthSettings>()?;
    Ok(res)
}
