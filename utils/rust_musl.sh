#!/bin/bash
docker run -it \
	-v cargo-cache:/root/.cargo/registry \
	-v cargo-git-cache:/root/.cargo/git \
	-v $(dirname $SSH_AUTH_SOCK):$(dirname $SSH_AUTH_SOCK) \
	-e SSH_AUTH_SOCK=$SSH_AUTH_SOCK \
	-v $(realpath ..):/volume \
	--rm \
	clux/muslrust \
	"$@"
