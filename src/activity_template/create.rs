use super::*;

/// Create a new activity constraint template.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The activity id for which to create the activity constraint template
/// - `constraint` - The constraint to create
///
/// # Return Value
///  The created activity constraint
pub fn create_activity_constraint_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    constraint: ConstraintCreator,
) -> Result<SelectActivityConstraintTemplate, db::error::IqiperDatabaseError> {
    match constraint {
        ConstraintCreator::Timer(x) => activity_constraint_template::create(
            conn,
            uid,
            activity_constraint_template::NewActivityConstraintTemplate {
                type_: ActivityConstraintTemplateType::Timer,
                activity_template_id: *activity_template_id,
                start_offset: x.start_offset,
                end_offset: x.end_offset,
                notify_offset: x.notify_offset,
                alert_message: x.alert_message,
            },
        ),
    }
}

/// Create a new activity contact template.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_template_id` - The activity id for which to create the activity contact template
/// - `contact_id` - The contact to link
///
/// # Return Value
/// The created activity contact
pub fn create_activity_contact_template(
    conn: &PgConnection,
    uid: &Uuid,
    activity_template_id: &Uuid,
    contact_id: Uuid,
) -> Result<ActivityContactTemplate, db::error::IqiperDatabaseError> {
    activity_contact_template::create(
        conn,
        uid,
        activity_contact_template::NewActivityContactTemplate {
            activity_template_id: *activity_template_id,
            contact_id: contact_id,
        },
    )
}

/// Create a new activity template.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `new_activity` - The object to create
///
/// # Return Value
/// The newly created activity
async fn create_activity_template(
    conn: &PgConnection,
    new_activity_template: NewActivityTemplate,
) -> Result<IdTsResponse, ResponseError> {
    Ok(activity_template::create(conn, new_activity_template)?)
}

/// POST /model/activity
/// Create a new activity
#[iqiper_mains_route(method = "post", route = "/model/activity")]
async fn route_create_activity_template(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    data_valid: web::Data<ValidatorActivityTemplate>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::Create, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let activity_obj = NewActivityTemplateWithoutUid::deserialize(body.0)?;
    let activity_obj = NewActivityTemplate {
        uid,
        name: activity_obj.name,
        icon: activity_obj.icon,
        alert_message: activity_obj.alert_message,
    };
    Ok(HttpResponse::Created().json(create_activity_template(&connection, activity_obj).await?))
}

/// POST /model/activity/{id}/contact/{cid}
/// Create a new activity contact
#[iqiper_mains_route(method = "post", route = "/model/activity/{id}/contact/{cid}")]
async fn route_create_activity_contact_template(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Ok().json(create::create_activity_contact_template(
            &connection,
            &uid,
            &path.id,
            path.cid,
        )?),
    )
}

/// POST /model/activity/{id}/constraint
/// Create a new activity constraint
#[iqiper_mains_route(method = "post", route = "/model/activity/{id}/constraint")]
async fn route_create_activity_constraint_template(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivityTemplate>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivityTemplate::CreateConstraint, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Created().json(create::create_activity_constraint_template(
            &connection,
            &uid,
            &path.id,
            ConstraintCreator::deserialize(body.into_inner())?,
        )?),
    )
}
