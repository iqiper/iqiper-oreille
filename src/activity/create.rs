use super::*;

/// Create a new activity constraint.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The activity id for which to create the activity constraint
/// - `constraint` - The constraint to create
///
/// # Return Value
///  The created activity constraint id and creation date
pub fn create_activity_constraint(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    constraint: ConstraintCreator,
) -> Result<IdTsResponse, db::error::IqiperDatabaseError> {
    match constraint {
        ConstraintCreator::Timer(x) => activity_constraint::create(
            conn,
            uid,
            activity_constraint::NewActivityConstraint {
                type_: ActivityConstraintType::Timer,
                activity_id: *activity_id,
                start_date: x.start_date,
                alert_message: x.alert_message,
                end_date: x.end_date,
                notify_date: x.notify_date,
            },
        ),
    }
}

/// Create a new activity contact.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `activity_id` - The activity id for which to create the activity contact
/// - `contact_id` - The contact to link
///
/// # Return Value
/// The created activity contact
pub fn create_activity_contact(
    conn: &PgConnection,
    uid: &Uuid,
    activity_id: &Uuid,
    contact_id: Uuid,
) -> Result<ActivityContact, db::error::IqiperDatabaseError> {
    activity_contact::create(
        conn,
        uid,
        activity_contact::NewActivityContact {
            activity_id: *activity_id,
            contact_id: contact_id,
        },
    )
}

/// Create a new activity.
///
/// # Arguments
/// - `conn` - The postgres connection
/// - `uid` - The uuid of the `distant_user` record
/// - `config` - What type of object should this function return.
/// - `new_activity` - The object to create
///
/// # Return Value
/// The newly created activity id and creation date
async fn create_activity(
    conn: &PgConnection,
    new_activity: NewActivity,
) -> Result<IdTsResponse, ResponseError> {
    Ok(activity::create(conn, new_activity)?)
}

/// POST /activity
/// Create a new activity
#[iqiper_mains_route(method = "post", route = "/activity")]
async fn route_create_activity(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    data_valid: web::Data<ValidatorActivity>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::Create, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    let activity_obj = NewActivityWithoutUid::deserialize(body.0)?;
    let activity_obj: NewActivity = NewActivity {
        uid,
        name: activity_obj.name,
        icon: activity_obj.icon,
        alert_message: activity_obj.alert_message,
        start_date: activity_obj.start_date,
    };
    Ok(HttpResponse::Created().json(create_activity(&connection, activity_obj).await?))
}

/// POST /activity/{id}/contact/{cid}
/// Create a new activity contact
#[iqiper_mains_route(method = "post", route = "/activity/{id}/contact/{cid}")]
async fn route_create_activity_contact(
    token: CoreJWTAuth<JWTClaims>,
    path: web::Path<IdAndCid>,
    data_server: web::Data<ServerState>,
) -> Result<HttpResponse, ResponseError> {
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(HttpResponse::Ok().json(create::create_activity_contact(
        &connection,
        &uid,
        &path.id,
        path.cid,
    )?))
}

/// POST /activity/{id}/constraint
/// Create a new activity constraint
#[iqiper_mains_route(method = "post", route = "/activity/{id}/constraint")]
async fn route_create_activity_constraint(
    token: CoreJWTAuth<JWTClaims>,
    body: web::Json<Value>,
    path: web::Path<Id>,
    data_server: web::Data<ServerState>,
    data_valid: web::Data<ValidatorActivity>,
) -> Result<HttpResponse, ResponseError> {
    let validator = data_valid.get_ref();
    validator.validate(&KindActivity::CreateConstraint, &body)?;
    let connection_lock = data_server.get_ref().db_pool_read();
    let connection = connection_lock
        .get()
        .map_err(|_| ResponseError::InternalServerError)?;
    let uid = extract_uid(&connection, &token)?;
    Ok(
        HttpResponse::Created().json(create::create_activity_constraint(
            &connection,
            &uid,
            &path.id,
            ConstraintCreator::deserialize(body.into_inner())?,
        )?),
    )
}
